fileR = open("trimmed-words", "r")
fileW = open("final.txt", "w")
count = 0
for line in fileR:
    for character in line:
        if(character == '\n'):
            fileW.write("'/n',")
            count = count + 1
            fileW.write("\n")
        else:            
            fileW.write("'")
            fileW.write(character)
            count = count + 1
            fileW.write("',")
            
fileW.write(str(count))
fileR.close()
fileW.close()
