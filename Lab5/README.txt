ENEL 487 - Lab 5
Names: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
Project Name: Lab 5

Date Started: October 19, 2017
Date Finished: October 25, 2017

How to run: in uVision, compile and build.
Tab length: four spaces
	
    
///////////////////////////////////////////////////////////////////////////////
// Description
///////////////////////////////////////////////////////////////////////////////

This program is meant to act as a CLI (Command Line Interface), this means
that the user is capable of providing inputs through a command line and we
will execute different tasks (depending on the input). Our program accepts
the following inputs as valid:

1) help
2) compiled
3) led #led on
4) led all on
5) led #led off
6) led all off
7) led #led query
8) timing

These inputs can are case insensative (meaning you can mix in capitals) and 
will also account for excess white space between commands. If the user enters
any of these valid commands, and task will be executed by the STM board. If the
input provided is invalid, they will recieve an error message and be prompted to 
enter another command. 

It should be noted that the valid range for #led is 8-15, this corresponds with 
our LEDs tied to PB8-PB15. 

///////////////////////////////////////////////////////////////////////////////
// Test Results
///////////////////////////////////////////////////////////////////////////////

This section will include the test results for Lab 3

Level 0/1/2/3 + optimized for time:
1) Add_32 is not optimized out
2) Add_64 is not optimized out
3) Multi_32 is not optimized out
4) Multi_64 is not optimized out
5) Div_32 is not optimized out
6) Div_64 is not optimized out 
7) Struct_8 is not optimized out
8) Struct_128 is not optimized out
9) Struct_1024 is not optimized out


///////////////////////////////////////////////////////////////////////////////
// Files Included
///////////////////////////////////////////////////////////////////////////////

The format will be as follows.

///////////////////////////////////////////////////////////////////////////////
/File Name
///////////////////////////////////////////////////////////////////////////////
Description: 
....
...
..
.
	Functions
	
///////////////////////////////////////////////////////////////////////////////
CLI.h & CLI.c : 
///////////////////////////////////////////////////////////////////////////////

The purpose of serial.h is to hold function
headers/declerations for all things related to our CLI. This
includes everything from recieving user input, parsing input,
and calling functions to execute valid commands given by the
user. In addition to functions that help us achieve the
aforementioned items we have list of defines, and 'string'
declerations to make everything in the .c file much cleaner.
			 
All define values are related to different ascii values, each
name is fairly descriptive. As an example we can see below
				 
ALPHA_LOWER_LIMT   -> 0x41, first capital letter on ascii table
ALPHA_MIDDLE_LIMIT -> 0x61, first lower case letter
ALPHA_UPPER_LIMIT  -> 0x7A, last lower case letter
				 
You can find the ascii table we utilized by navigating to the 
page listed below.
				 
http://www.asciitable.com/
	
    bufferOverflow	
	combineStrings
	newLine
	returnLine
	enterLine
	welcomeMessage
	printString
	printWithTab
	printChar
	printHelp
	printBlueSky
	printTiming
	printCompiled
	printQuery
	recieveInput
	cutSpacing
	parseInput
	handleInput
	toUpper
	strLen
	strToInt
	checkCommand
	checkSubCommand
	checkLedNum
	checkArrayCompare
	checkAlphaCap
	checkAlphaLow
	checkNumeric
	printInt
	power
	
///////////////////////////////////////////////////////////////////////////////
register.h:
///////////////////////////////////////////////////////////////////////////////

The purpose of registers.h is to declare values that relate to
address values for each STM peripheral we use. Having these 
defintions makes it much easier to refer to our desired peripheral
without having to look up its register value every time.

///////////////////////////////////////////////////////////////////////////////
ledControl.h & ledControl.c:
///////////////////////////////////////////////////////////////////////////////

The purpose of ledControl.h is to hold function headers/declerations
for all things related to controlling the LEDs on the STM board.
We also define an array of different numbers, these numbers were 
selected by referring to the BSRR register map in the STM reference
manual. Each value is fairly descriptive, but below is an example
of how we obtained these values.
 
PB8_ON ->  0x00000100, this is a 32bit number of all 0's except for
           position 9. Next refer to page 172 of the reference manual
           rev 16. We will see that the 9th position is mapped to BS8.
           This register when set will put PB8 high, and if an LED
           is attached then it will turn on.
				 
PB8_OFF -> 0x01000000, this is a 32 bit number of all 0's except for
           postiion 24. Once again refer to page 172 of ref manual 
           rev 16. We will see that the 24th position is mapped to BR8
           , when set this will put PB8 low, and if an LED is attached
           it will turn off.

    ledInit
    ledOn
    ledOff
    allLedOn
    allLedOff
    queryLed

///////////////////////////////////////////////////////////////////////////////
serial.h & serial.c
///////////////////////////////////////////////////////////////////////////////

The purpose of serial.h is to hold function headers/declerations
for all things related to sending/reciving information through
USART on the STM board. In addition to these functions we also set
values through defines. These values were determined by using the
STM reference manual rev 16 (provided in class). Below is a small
example of how a value would be determined.

APB2ENR_GPIOA -> Port A is the bus in which pins mapped to USART2
                 exist, as such we will need to enable GPIOA for user.
                 we set this value to 0x00000004, this is a 32 bit
                 number with all 0's except for position 3. If we
                 refer to our manual we will see that for 
                 RCC_APB2ENR register located on page 145, position 3
                 is named IOPAEN. Meaning enable input/output for
                 port A.
				 
     openUsart
     getByte
     sendByte

///////////////////////////////////////////////////////////////////////////////
timer.h, timer_impl.h, timer_ops.c & timer_impl.c
///////////////////////////////////////////////////////////////////////////////

The purpose of serial.h is to hold function headers/declerations
for all functions related to timing. timer_impl.h holds any defined
values used by timing functions.  timer_impl.c includes definitions for
any simple timing related functions that work with timers at a register level.
timer_ops includes other timing related functions that utilize the basic timing
functions to measure the time taken to perform various operations.
				 
     timer_init
	 timer_init_with_interrupts
	 timer_start
	 timer_stop
	 intToUint
	 timer_shutdown
	 randomGenerator_8Byte
	 randomGenerator_128Byte
	 randomGenerator_1024Byte
	 randomGenerator_32
	 randomGenerator_64
	 Add32
	 runTimeAll
	 beginTiming

///////////////////////////////////////////////////////////////////////////////
servo.c servo.h ADC.c ADC.h
///////////////////////////////////////////////////////////////////////////////

The purpose of servo.h is to hold function headers/declerations for all things
related to our PWM servo control. In the same manner servo.c holds all the
expanded code to execute these functions. ADC.h contains all the headers/
declerations associated with our IR Sensor ADC sensing. Finally ADC.c holds
all the fleshed out code for obtaining data from our ADC.

functions listed below are servo related, then ADC related.

	servo_init
	servoSetPercent
	servoSetDegrees

	adcInit
	adcStartCapture
	adcReadValue

///////////////////////////////////////////////////////////////////////////////
// Version History
///////////////////////////////////////////////////////////////////////////////

Version 1.0: This version of our program was fairly simple. It retained 
             portions of ledControl.h/.c that you see now. It simply 
             flashed our LEDs (PB8-PB15) in a fashion that was given
             by Karim in the class.
             *There were no revisions made after the first run.
			 
             Started: September 21, 2017
             Completed: September 21, 2017
			 
Version 2.0: In v2 we improved v1 by making it more modular. This was achieved
             by creating functions that accepted numeric inputs as LED values.
             By doing this we prepared out system to accept a user entering a
             LED value.
			 
             Started: September 28, 2017
             Completed: September 28, 2017
			 
Version 2.1: in v2.1 we added in the serial.h/.c files which introduced USART
             functionaility to our program. However, upon adding these several
             bugs were encountered. These bugs were fixed in later versions, 
             in this version we simply introduced the files, and mapped
             regiester values.
			 
             Started: October 5, 2017
             Completed: October 5, 2017
			
Version 2.2: in v2.2 we fixed all the bugs of v2.1, and got USART working. We
             had a welcome message printing on the screen, and input was being
             stored into a char*.
			 
             Started: October 9, 2017
             Completed: October 9, 2017
			 
Version 2.3: v2.3 saw the completion of CLI interface. Input collecting,
             parsing values, and calling functions was completed. The CLI
             was working as expected. Error checks were added in, user was
             not allowed to backspace empty inputs, user could backspace 
             regular inputs, white spaces were accounted for.
			 
             Started: October 11, 2017
             Completed: Otober 12, 2017
			 
Version 3.0: v2.3 added the timing command to the existing CLI which uses the
			 the board's built in timer to determine the number of clock ticks
			 required to perform a number of operations.
			 
             Started: October 12, 2017
             Completed: Otober 19, 2017
			 
Version 3.1: An addition to v3.0 was integrating a interrupt driven LED that
             will flash every 1 second. In the future I plan to add a CLI
			 command that controls this leds timer blinking on/off.
			 
			 Started: October 18, 2017
             Completed: Otober 19, 2017
			 
Version 4.0: A completly new version following 3.1, has the added features of
             PWM to control a servo. You are able to enter commands using 
			 degrees and precent to adjust the servo in any way you desire.
			 
			 Started: October 19, 2017
			 Finished: October 21, 2017
			 
Version 4.1: In addition to the PWM servo controls, a ADC for measuring values
             given by an IR sensor was added. The user can view the raw
			 distance value of the ball by using the command READ
			 
			 Started: October 25, 2017
			 Finished: October 25, 2017
