/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 &
File Name: ledControl.c
Date Started: September 28, 2017 
Date Finished: September 28, 2017

Purpose: The purpose of ledControl.c is to control all things related to LED
         operations. In this file we have a function to initialize use of
				 our LEDs (which are located PB8-PB15). All other functions are 
				 used to toggle on/off different LEDs. For more information about
				 individual functions, please refer to their decleration in 
				 ledControl.h .
				 
For detailed function discriptions please refer to ledControl.h where the
function is declared. Below is a name list for the utilized functions

Functions: ledInit
           ledOn
					 ledOff
					 allLedOn
					 allLedOff
					 queryLed
					 queryAllLed
*/
#include "registers.h"
#include "ledControl.h"


void ledInit ()
{
	// Cofigure registers to control LEDs
	
  RCC_APB2ENR |= APB2ENR_GPIOB; // Enable Port B clock
	GPIOB_ODR  &= ~GPIOB_OFF;  // switch off outputs that are not 8-15 port B
	GPIOB_CRH  = OUT_TYPE; //50MHz  General Purpose output push-pull (open-drain)
	
	return;
}

// Function utilizes a simple switch case to assign different values
// to the BSRR register. Refer to ledControl.h for a brief explination
// of operation.
void ledOn (unsigned led)
{
	switch (led)
	{
		case 8: 
			GPIOB_BSRR = PB8_ON;
			break;
		
		
		case 9:
			GPIOB_BSRR = PB9_ON;
			break;
		
		case 10:
			GPIOB_BSRR = PB10_ON;
			break;
		
		case 11:
			GPIOB_BSRR = PB11_ON;
			break;
		
		case 12:
			GPIOB_BSRR = PB12_ON;
			break;
		
		case 13:
			GPIOB_BSRR = PB13_ON;
			break;
		
		case 14:
			GPIOB_BSRR = PB14_ON;
			break;
		
		case 15:
			GPIOB_BSRR = PB15_ON;
			break;
		
		default: 
			break;
	}
}

// Function utilizes a simple switch case to assign different values
// to the BSRR register. Refer to ledControl.h for a brief explination
// of operation.
void ledOff (unsigned led) 
{
	switch (led)
	{
		case 8: 
			GPIOB_BSRR = PB8_OFF;
			break;
		
		case 9:
			GPIOB_BSRR = PB9_OFF;
			break;
		
		case 10:
			GPIOB_BSRR = PB10_OFF;
			break;
		
		case 11:
			GPIOB_BSRR = PB11_OFF;
			break;
		
		case 12:
			GPIOB_BSRR = PB12_OFF;
			break;
		
		case 13:
			GPIOB_BSRR = PB13_OFF;
			break;
		
		case 14:
			GPIOB_BSRR = PB14_OFF;
			break;
		
		case 15:
			GPIOB_BSRR = PB15_OFF;
			break;
		
		default:
			break;
	}
}
	

// Function utilizes a simple switch case to assign different values
// to the BSRR register. Refer to ledControl.h for a brief explination
// of operation.
void allLedOn (void) 
{
	GPIOB_BSRR = PBALL_ON;
}

// Function utilizes a simple switch case to assign different values
// to the BSRR register. Refer to ledControl.h for a brief explination
// of operation.
void allLedOff (void) 
{
	GPIOB_BSRR = PBALL_OFF;
}

// Function shifts the ODR register by the number given, and the checks if
// the given value is 1 or not. Since we are looking at the ODR (Output 
// data register) we will know if an led is on/off depending on that 1 or 0.
unsigned queryLed (unsigned ledNum)
{
	if ((GPIOB_ODR >> ledNum) & 0x1)
		return 1;
	return 0;
}

// This function queries every single one of our leds from PB8 -> PB15 if any
// of them are turned off it sets to zero and returns.
unsigned queryAllLed () 
{
	int i;
	
	for (i = 0; i < NUM_LED; i++)
	{
		if (!((GPIOB_ODR >> (i + NUM_LED)) & 0x1))
			return 0;
	}
	return 1;
}
