/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: servo.h
Date Started: November 2, 2017 
Date Finished: November 9, 2017 

Purpose: This file contains function declarations for a control system to set
         the height of the ping-pong ball
*/

#include "servo.h"
#include "adc.h"

#define CONVERSION_SCALAR              20000
#define CONVERSION_OFFSET              500
#define MAX_CM                         65
#define MIN_CM                         6
#define HIGH_RANGE                     750
#define MID_RANGE                      1100
#define HIGH_GAIN                      5
#define MID_GAIN                       10
#define LOW_GAIN                       20

#define FULL_CLOSE                     1200
#define NEUTRAL                        980
#define FULL_OPEN                      826

extern uint32_t global_control_set_ball_raw;
extern uint16_t global_adc_1_value;
extern unsigned global_adc_new_value;

/**
Name: setBallFloat
Input: uint32_t
Output: void
Purpose: Sets the height of the ball in cm
*/
void setBallFloat (uint32_t);

/**
Name: cmToRaw
Input: uint32_t
Output: uint32_t
Purpose: Converts cm to the equivalent number of raw ticks
*/
uint32_t cmToRaw (uint32_t);

/**
Name: updateController
Input: void
Output: void
Purpose: Reads the ACD and sets the value position accordingly
*/
void updateController (void);

/**
Name: setControlLevel
Input: int
Output: void
Purpose: Adjusts the servo position to raise or lower the ball height
*/
void setControlLevel (int);


