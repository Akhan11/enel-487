/**

Assignment 2
ENEL 487
Arsalan Khan
200343820

started: October 03, 2017
finished: October 04, 2017

The purpose of this program is to calculate the sum, difference, multiple, quotient of
a given complex number. The user will provide an input that consists of a character indicating
the operation (A, S, M, D), followed by four floats representing two sets of complex numbers. These numbers are
represented in rectangular form0. They will also be allowed to quit the program, and will get error messages.

In addition to the operators provided above, the program will also give users the option of entering the following:
- abs
- arg
- argDeg
- exp
- inv

All of these commands will expect two floats as inputs, and will output different things depending on the function (refer to comp.h/comp.cpp)
for function definitions.

*/

#include <iostream>
#include "comp.h"
#include "stdio.h"
#include <string.h>
#include <stdbool.h>

using namespace std;

int main()
{
	// Variable decleration/initializations, they are fairly straight forward
	Complex input1 = { 0,0 }, input2 = { 0,0 }, result = { 0,0 };
	float altResult;

	char userInput[100] = "";
	char operation[10] = "";
	string finalOperation;
	char expectEnd;

	bool correctOperator;
	bool singleAnswer;
	
	

	cerr << "Type a letter to specify the arithmetic operator (A, S, M, D)\n";
	cerr << "followed by two complex numbers expressed as pairs of doubles.\n";
	cerr << "Or enter abs, arg, argDeg, exp, inv followed one complex number\n";
	cerr << "to represent a single complex number\n";
	cerr << "Type Q to quit.\n";

	cerr << "\nEnter exp: ";

	do {
		//retrieve and store user input
		fgets(userInput, 100, stdin);
		sscanf(userInput, "%s %f %f %f %f %[^\n]", operation, &input1.real, &input1.imaginary, &input2.real, &input2.imaginary, &expectEnd);

		//loop through the char array for user input on operation and turn all characters to uppercase (less cases to check later on)
		for (unsigned i = 0; i < strlen(operation); i++)
		{
			operation[i] = toupper(operation[i]);
		}
		// cast the char array to a string
		finalOperation = string(operation);

		//simple logic for calling the correct function, depending on user input
		if ((finalOperation != "q" && finalOperation != "Q")) {
			correctOperator = true;
			singleAnswer = false;
			if (finalOperation == "A")
			{
				result = complexAddition(input1, input2);
			}
			else if (finalOperation == "S")
			{
				result = complexSubtract(input1, input2);
			}
			else if (finalOperation == "M")
			{
				result = complexMultiply(input1, input2);
			}
			else if (finalOperation == "D")
			{
				result = complexDivide(input1, input2);
			}
			else if (finalOperation == "ABS")
			{
				singleAnswer = true;
				altResult = complexAbs(input1);
			}
			else if (finalOperation == "ARG")
			{
				singleAnswer = true;
				altResult = complexArg(input1);
			}
			else if (finalOperation == "ARGDEG")
			{
				singleAnswer = true;
				altResult = complexArgDeg(input1);
			}
			else if (finalOperation == "EXP")
			{
				result = complexExp(input1);
			}
			else if (finalOperation == "INV")
			{
				result = complexInv(input1);
			}
			else
			{
				correctOperator = false;
			}
			printValue(correctOperator, singleAnswer, result, altResult);
			cerr << "\nEnter exp: ";
		}
		else 
		{
			cerr << "Not the correct input";
		}
	} while (finalOperation != "Q");

	return 0;
}
