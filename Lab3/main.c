/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 &
File Name: main.c
Date Started: September 28, 2017 
Date Finished: October 12, 2017

Purpose: The purpose of this file is to make function calls that will allow
         our CLI interface to run seamlessly. Main itself will only be
				 responsible for declaring intial values, so that data may be
				 stored in them at a later point. 
				 
				 At a very high level our CLI interface works in three steps:
				 
				 1) Recieve input from the user
				 2) Parse/Analyze the recieved string into useable values
				 3) Take the cleaned up string, and preform a task if a valid
				    expression has been entered. Otherwise display error message

         For a more elaborate explination of allowed inputs, how the input
				 is parsed, and what valid expressions are please check the README.
				 Another option is to check the function decleration in its respective
				 .h file, there you will find a detailed explination of everything to
				 do with that function.
*/
#include "serial.h"
#include "ledControl.h"
#include "CLI.h"
#include "timer.h"

int main()
{	
	char userInput [MAX_USER_INPUT];
	char command [MAX_USER_INPUT];
	char subCommand [MAX_USER_INPUT];
	char ledNum [MAX_USER_INPUT];
	char expectEnd [1] ;
	uint32_t avgTime[NUM_OF_TESTS] = {0};
	uint32_t timeNullAvg = 0;
	
	openUsart();
	ledInit ();
	timer_init ();
	
	welcomeMessage();	
	while (1)
	{
		recieveInput (userInput);
		returnLine ();
		parseInput (userInput, command, ledNum, subCommand, expectEnd);
		handleInput (command, ledNum, subCommand, expectEnd, avgTime,
		             timeNullAvg);
	}
}

void TIM3_IRQHandler ()
{
	
	if (charArrayCompare (ledInterupt, "ALL"))
	{
		if (queryAllLed ())
			allLedOff ();
		else
			allLedOn ();
	}
	else if (charArrayCompare (ledInterupt, "8"))
	{
		if (queryLed (LED_8))
			ledOff (LED_8);
		else
			ledOn (LED_8);
	}
	else if (charArrayCompare (ledInterupt, "9"))
	{
		if (queryLed (LED_9))
			ledOff (LED_9);
		else
			ledOn (LED_9);
	}
	else if (charArrayCompare (ledInterupt, "10"))
	{
		if (queryLed (LED_10))
			ledOff (LED_10);
		else
			ledOn (LED_10);
	}
	else if (charArrayCompare (ledInterupt, "11"))
	{
		if (queryLed (LED_11))
			ledOff (LED_11);
		else
			ledOn (LED_11);
	}
	else if (charArrayCompare (ledInterupt, "12"))
	{
		if (queryLed (LED_12))
			ledOff (LED_12);
		else
			ledOn (LED_12);
	}
	else if (charArrayCompare (ledInterupt, "13"))
	{
		if (queryLed (LED_13))
			ledOff (LED_13);
		else
			ledOn (LED_13);
	}
	else if (charArrayCompare (ledInterupt, "14"))
	{
		if (queryLed (LED_14))
			ledOff (LED_14);
		else
			ledOn (LED_14);
	}
	else if (charArrayCompare (ledInterupt, "15"))
	{
		if (queryLed (LED_15))
			ledOff (LED_15);
		else
			ledOn (LED_15);
	}
	
	TIM3_SR = 0;
}



