/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: servo.c
Date Started: October 19, 2017 
Date Finished: October , 2017 

Purpose: This file contains function definitions for pwm control of a servo
         motor.  The servo is connected to PB7 and the functions use channel
				 two of timer four to generate a PWM signal.
				 
///////////////////////////////////////////////////////////////////////////////
// For detailed function discriptions please refer to servo.h where the function
// is declared. Below is a name list for the utilized functions.
// This files comments will simply explain the code
///////////////////////////////////////////////////////////////////////////////

Functions: servo_init
           servoSetPercent
					 servoSetDegrees


*/
#include "servo.h"

// Initializes the output required to run the servo and configures TIM4
// to provide a PWM signal.
void servo_init (void)
{
	// Enable clock for GPIOB
	RCC_APB2ENR |= RCC_APB2ENR_GPIOB;
	
	// Configure PB7 to output mode, 10MHz, alternate function open drain
	GPIOB_CRL &= ~GPIOB_CRL_PB7_MASK;
	GPIOB_CRL |= GPIOB_CRL_PB7_AFIO;
	
	pwm_init ();
}

void servoSetRaw (uint32_t raw)
{
	if ((raw <= MAX_TICKS) && (raw >= MIN_TICKS))
		pwmCCRSet (raw);
}

// Simple function that sets our pwm depending on the users input. It will
// not allow a change in pwm if it is beyong our max allowed precent
void servoSetPercent (uint32_t percent)
{
	if (percent <= MAX_PERCENT)
		pwmCCRSet ((percent * ((MAX_TICKS - MIN_TICKS)/MAX_PERCENT))
			+ MIN_TICKS);
}

// Simple function that sets our pwm depending on the users input. It will
// not allow a change in pwm if it is beyong our max allowed degree
void servoSetDegrees (uint32_t degrees)
{
	if (degrees <= MAX_DEGREES)
		pwmCCRSet ((degrees * ((MAX_TICKS - MIN_TICKS)/MAX_DEGREES)) + MIN_TICKS);
}
