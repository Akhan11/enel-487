/**
   Initialize the timer TIM4.
*/
#include "minimal_regs.h"
void stm32_TimerSetup (void) {

    RCC_APB1ENR |= 1u<<2;	// 1: enable clock for TIM4

    TIM4_PSC = 1; //__PSC(__TIMXCLK, __TIM4_PERIOD); //1  set prescaler
    TIM4_ARR = 35999; //__ARR(__TIMXCLK, __TIM4_PERIOD); //35999  set auto-reload

    TIM4_CR1 = 0;		// reset command register 1
    TIM4_CR2 = 0;		// reset command register 2


    TIM4_PSC = 143; 
    TIM4_ARR = 9999; // __TIM4_ARR;   //0x270f set auto-reload

    TIM4_CCR1  = 0;      //__TIM4_CCR1;  //0
    TIM4_CCR2  = 0x4B0;  //__TIM4_CCR2;  //0
    TIM4_CCR3  = 0;      //__TIM4_CCR3;  //0x1388
    TIM4_CCR4  = 0;      //__TIM4_CCR4;  //0x09c4
    TIM4_CCMR1 = 0x6000; //__TIM4_CCMR1; //0
    TIM4_CCMR2 = 0;      //__TIM4_CCMR2; //0x6060
    TIM4_CCER  = 0x0010; //__TIM4_CCER;  //0x1100 set capture/compare enable register
    TIM4_SMCR  = 0;      //__TIM4_SMCR;    // 0 set slave mode control register

    TIM4_CR1 = 1u<<2;    // 1: URS: Only counter overflow/underflow
			  // generates an update interrupt or DMA
			  // request if enabled.

    TIM4_CR2 = 0;        //__TIM4_CR2; // 0x0 set command register 2


    TIM4_CR1 |= 1u<<0;  // 0: enable timer

} // end of stm32_TimSetup


