/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: timer.h
Date Started: October 12, 2017 
Date Finished: October 18, 2017 

Purpose: The purpose of this file is to provide function declerations
         for all things related to timers & timer processing. These functions
				 will do tasks such as initializing timers, measuring time taken for
				 different functions, and generate random numbers to be used for these
				 timing instructions.
*/
#pragma once

#include "timer_impl.h"
#include "registers.h"

/**
Name: timer_init
Input: void
Output: void
Purpose: Configures registers to set up TIM2
*/
void timer_init (void);

/**
Name: timer_init_with_interrupts
Input: void
Output: void
Purpose: Configures registers to set up TIM3 to fire an
         an interrupt at 1 Hz
*/
void timer_init_with_interrupts (void);

/**
Name: pwm_init
Input: void
Output: void
Purpose: Configures registers to set up TIM4 channel 2 as 
         a pwm channel
*/
void pwm_init (void);

/**
Name: pwmCCRSet
Input: void
Output: uint16_t
Purpose: Sets the number of ticks to turn pwm on for
*/
void pwmCCRSet (uint16_t ticks);

/**
Name: timer_start
Input: void
Output: int16_t
Purpose: Returns the value of the timer counter
*/
int16_t timer_start (void);

/**
Name: timer_stop
Input: int16_t
Output: int16_t
Purpose: Returns the number of clock cycles elapsed since value passed
*/
int16_t timer_stop(int16_t);

/**
Name: intToUint
Input: int16_t
Output: uint16_t
Purpose: Converts a 16-bit int to a 16-bit uint preserving the bit pattern
*/
uint32_t intToUint (int16_t);

/**
Name: timer_shutdown
Input: void
Output: void
Purpose: Shuts down timer resets configuration regiseters
*/
void timer_shutdown(void);

/**
Name: randomGenerator_8Byte
Input: Byte_8
Output: Byte_8
Purpose: Generates a random number of size 8 bytes
*/
Byte_8 randomGenerator_8Byte (Byte_8);

/**
Name: randomGenerator_128Byte
Input: Byte_128
Output: Byte_128
Purpose: Generates a random number of size 128 bytes
*/
Byte_128 randomGenerator_128Byte (Byte_128);

/**
Name: randomGenerator_32
Input: None
Output: 32 bit int
Purpose: Generates a random integer of 32 bits
*/
int32_t randomGenerator_32 (void);

/**
Name: randomGenerator_64
Input: None
Output: 64 bit int
Purpose: Generates a random integer of 64 bits
*/
int64_t randomGenerator_64 (void);

/**
Name: Add32
Input: void
Output: 16 bit unsigned int
Purpose: This will return the time taken to add two 32 bit integers once
*/
uint16_t Add32 (void);

/**
Name: beginTiming
Input: uint16_t[], uint32_t
Output: uint32_t
Purpose: The purpose of this function is to begin the testing for our different
         operations. This function will take an input of a uint32_t array, 
				 which stores the avg time value required to do a function. As well as
				 a uint32_t which stores the null time taken. Using nested for loops we
				 will execute each process for a total tests indicated by 
				 NUM_OF_TRIALS.
*/
uint32_t beginTiming (uint32_t[], uint32_t);
