/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: ADC.h
Date Started: October 28, 2017 
Date Finished: October , 2017 

Purpose: This file contains function declarations for the ADC block.
*/
#include "registers.h"

extern unsigned global_adc_new_value;

#define RCC_APB2ENR_ADC1       0x00000200
#define RCC_APB2ENR_GPIOC      0x00000010
#define GPIOC_CRL_PC0_MASK     0x00000004
#define SQ1_MASK               0xA
#define SMPR1_MASK             0x5
#define EXT_TRIGGER            0x100000
#define EXT_TRIG_SEL           0xE0000
#define ADC_ON                 0x1
#define SCAN_MASK              0x100
#define EOCIE_MASK             0x20
#define SWSTART_MASK           0x400000
#define RESET_CAL              0x8
#define CAL                    0x4
#define NVIC_BIT_18            0x40000
#define DR_MASK                0xFFFF

/**
Name: adcInit
Input: void
Output: void
Purpose: Configures registers to set up ADC for obtaining data from our IR
				 Sensor
*/
void adcInit (void);

/**
Name: adcStartCapture
Input: void
Output: void
Purpose: This function starts the capture process and sets our global flag
         low, indicating that we are trying to obtain a new value.
*/
void adcStartCapture (void);

/**
Name: adcReadValue
Input: void
Output: uint16_t
Purpose: Simply returns the value stored within the DR of ADC
*/
uint16_t adcReadValue (void);
