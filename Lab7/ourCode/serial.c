/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 &
File Name: serial.c
Date Started: September 28, 2017 
Date Finished: October 9, 2017

Purpose: The purpose of serial.c is to control all things related to our CLI.
         We enable USART to be able to send and recieve data at a baudrate of
				 9600.

For detailed function discriptions please refer to serial.h where the function
is declared. Below is a name list for the utilized functions

Functions: openUsart
           getByte
					 sendByte
*/
#include "serial.h"
#include "registers.h"



// Setting registers for Usart, but not intializing it
void openUsart (void)
{
	RCC_APB2ENR |= APB2ENR_GPIOA; // Enable clock for GPIOA, set bit 3
	RCC_APB1ENR |= USART_ENABLE; // Sets bit 17 of APB1ENR which relates to USART2EN

	// Clear the registers before setting them
	USART2_CR1 = 0x00;
	USART2_CR2 = 0x00;
	USART2_CR3 = 0x00;

	
	GPIOA_CRL &= ~(0xFFUL << 8); // clear PA2 and PA3
	GPIOA_CRL |= (0x0BUL << 8); // Sets PA2 as Alternate function output, push pull.
	GPIOA_CRL |= (0x04UL << 12); // Sets PA3 as a floating input (also the reset state).
	
	// sets the baud rate to 9600, value obtained from refernce manual
	USART2_BRR = 0xEA6; 

	/**
	Set bit 13: Enable USART
	Set bit 2: Enable Recieve
	Set bit 3: Enable Transmit
	Set bit 5: Enable RXNE interrupts

	Clear bit 12: Sets word length (1 Start bit, 8 Data bits, n Stop bit)
	Clear bit 10: Disables parity control
	
	*/
	USART2_CR1 |= (0x02UL << 12); // sets the USART enable bit
	// Sets the RE & TE bits for enabling the reciever/transmitter.
	// Also enable interrupts on RXNE
	USART2_CR1 |= 0x2C;
	
	NVIC_ISER_1 |= NVIC_BIT_38;
}

void sendByte (char bit)
{
	// while the transmit data register is not empty, do not send anything
	while (!(USART2_SR & USART_SR_TXE)){}
	USART2_DR = bit;
}

char getByte (void)
{
	// while the read data register is not empty, do not try to read anything
	return USART2_DR;
}
