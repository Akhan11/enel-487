/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 &
File Name: serial.h
Date Started: September 28, 2017 
Date Finished: October 5, 2017 

Purpose: The purpose of serial.h is to hold function headers/declerations
         for all things related to sending/reciving information through
				 USART on the STM board. In addition to these functions we also set
				 values through defines. These values were determined by using the
				 STM reference manual rev 16 (provided in class). Below is a small
				 example of how a value would be determined.
				 
				 APB2ENR_GPIOA -> Port A is the bus in which pins mapped to USART2
				                  exist, as such we will need to enable GPIOA for user.
													we set this value to 0x00000004, this is a 32 bit
													number with all 0's except for position 3. If we
													refer to our manual we will see that for 
													RCC_APB2ENR register located on page 145, position 3
													is named IOPAEN. Meaning enable input/output for
													port A.

All values indicated below were pulled from the STM reference manual rev 16.
*/
#pragma once

#include "registers.h"

// As mentioned above, this block of defines is used to set bits for 
// different registers
#define APB2ENR_GPIOA            ((uint32_t)0x00000004)
#define USART_ENABLE             ((uint32_t)0x00020000)
#define USART_SR_TXE             ((uint32_t)0x00000080)
#define USART_DR_DR              ((uint32_t)0x000001FF)
#define USART_SR_RXNE            ((uint32_t)0x00000020)

/**
Name: openUsart 
Input: None
Output: None
Purpose: This function initializes USART for use, and sets the baud rate to 9600.
*/
void openUsart (void);

/**
Name: sendByte 
Input: char
Output: None
Purpose: This function sends a character to the command line
*/
void sendByte (char);

/**
Name: getByte 
Input: None
Output: char
Purpose: This function recieves a character from the command line
*/char getByte (void);
