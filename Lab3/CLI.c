/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: CLI.c
Date Started: October 9, 2017 
Date Finished: October 19, 2017

Purpose: The purpose of this file is to control all things related to our CLI
         in terms of input/output handling, and performing valid commands
				 entered by the user. In addition to those CLI operations it will also
				 handle intializing a handful of char* that were not set in the 
				 CLI.h file due to line size restrictions.
				 
				 It should be noted that we did not make a seperate "print.c/.h" file
				 because printing, parsing, and performing tasks are all related
				 to our CLI. 

///////////////////////////////////////////////////////////////////////////////
// For detailed function discriptions please refer to CLI.h where the function
// is declared. Below is a name list for the utilized functions.
// This files comments will simply explain the code
///////////////////////////////////////////////////////////////////////////////

Functions: bufferOverflow
           combineStrings
					 
					 newLine
					 returnLine
					 enterLine
					 
					 welcomeMessage
					 printString
					 printWithTab
					 printChar
					 printHelp
					 PrintBlueSky
					 printTiming
					 printCompiled
					 printQuery
					 
					 recieveInput
					 
					 cutSpacing
					 parseInput
					 handleInput
					 toUpper
					 strLen
					 strToInt
					 
					 checkCommand
					 checkSubCommand
					 checkLedNum
					 checkArrayCompare
					 checkAlphaCap
					 checkAlphaLow
					 checkNumeric
					 
					 printInt
					 power
*/
#include "timer.h"
#include "serial.h"
#include "CLI.h"

char* ledInterupt = "";

// This function stops addition to our current buffer until a backspace or
// enter key is pressed.
char bufferOverflow (int length, char data)
{
	while ((data != CARRIAGE_RETURN) && (data != BACKSPACE) &&
         (data != DELETE))
	{
		data = getByte();
	}	
	return data;
}

// Combining two given char*
const char* combineStrings (const char* start, const char* end)
{
	char* combined;
	int i = 0;
	int j;
	// for loop to append the first char* to our result
	for (j = 0; j < strLen (start); i++, j++)
	{
		combined[i] = start[j];
	}
	// for loop to append the second char* to our result
	for (j = 0; j < strLen (end); i++, j++)
	{
		combined[i] = end[j];
	}
	//return the effective result of start + end
	return combined;
}

// send a '\n\ value to the command line
void newLine ()
{
	sendByte (NEW_LINE);
}

// Send a '\r' value to the command line
void returnLine ()
{
	sendByte (CARRIAGE_RETURN);
}

// send a '\n' & '\r' to the command line
void enterLine ()
{
	newLine ();
	returnLine ();
}

// send an assortment of strings to the command line as a welcome message
void welcomeMessage()
{
	printString (NAMES);
	returnLine ();
	printString (COPY);
	returnLine ();
	enterLine ();
}

// send a full 'string' to the command line
void printString (const char* message) 
{
	int i; 
	int stringLen = strLen (message);
	for (i = 0; i < stringLen; i++)
	{
		// will send one character of the string to the command line at a time
		sendByte (message[i]);
	}
}

// sends a two spaced tab, along with printing the indicated char*
void printWithTab (const char* message)
{
	printString (TAB);
	printString (message);
}

// send a passed character value to the command line
void printChar (char value)
{
	sendByte (value);
}

// print a batch of pre-existing 'strings' to the user to find proper
// formatting for commands
void printHelp (void)
{
	printWithTab (INSTRUCTIONS);
	enterLine ();
	printWithTab (HELP);
	returnLine ();
	printWithTab (DATE_TIME);
	returnLine ();
	printWithTab (LED_ON);
	returnLine ();
	printWithTab (LED_OFF);
	returnLine ();
	printWithTab (ALL_LED_ON);
	returnLine ();
	printWithTab (ALL_LED_OFF);
	returnLine ();
	printWithTab (LED_QUERY);
	returnLine ();
	printWithTab (TIMING);
	enterLine ();
}
void printBlueSky ()
{
	printString ("Please tell us why");
	enterLine ();
	printString ("You had to hide away for so long");
	enterLine ();
	printString ("(So long)");
	enterLine ();
}

// prints a batch of pre-existing 'strings' to the screen along with
// the times taken for each operation to execute + null time
void printTiming (uint32_t avgTime[], uint32_t timeNullAvg)
{
	printWithTab (TIMING_DISCLAIMER);
	enterLine ();
	printWithTab (NULL_TIME);
	printInt(timeNullAvg);
	enterLine ();
	printWithTab (ADD_32);
	printInt (avgTime[0]);
	enterLine ();
	printWithTab (ADD_64);
	printInt (avgTime[1]);
	enterLine ();
	printWithTab (MULTI_32);
	printInt (avgTime[2]);
	enterLine ();
	printWithTab (MULTI_64);
	printInt (avgTime[3]);
	enterLine ();
	printWithTab (DIV_32);
	printInt (avgTime[4]);
	enterLine ();
	printWithTab (DIV_64);
	printInt (avgTime[5]);
	enterLine ();
	printWithTab (COPY_8);
	printInt (avgTime[6]);
	enterLine ();
	printWithTab (COPY_128);
	printInt (avgTime[7]);
	enterLine ();
	printWithTab (COPY_1024);
	printInt (avgTime[8]);
	enterLine ();
}


// Print the date & time of when the project was compiled
void printCompiled (void)
{
	printWithTab (__DATE__);
	printWithTab (__TIME__);
	enterLine ();
}

// print whether or not an LED given by char* ledNum is on or off 
void printQuery (char* ledNum)
{
	printWithTab ("LED ");
	printString (ledNum);
	printString (" is ");
	
	if (queryLed (strToInt (ledNum)))
		printString ("on.\n");
	else
		printString ("off.\n");
}

// recieve user input as a single string of max length 20
void recieveInput (char message [])
{
	// we need to be sure to 0/null starting values so that there is no
	// garbage contained within them
	char data = ' ';
	int length = 0;
	message[0] = NULL_CHAR;
	// prompt user for intput
	printString (ENTER);
	// As long as we haven't reached the character limit, or user hasn't hit
	// enter, keep reciving input
	while (data != CARRIAGE_RETURN)
	{
		// obtain input
		data = getByte ();
		if (length >= MAX_USER_INPUT)
			data = bufferOverflow (length, data);
	
		// make sure its not a enter key press
		if (data != CARRIAGE_RETURN)
		{
			// check to see if they are deleting or backspacing a character
			if (data == DELETE || data == BACKSPACE)
			{
				// if there exists a string
				if (length != 0)
				{
					sendByte (data); // remove character from command line display
					length--; // decrement length to reflect the deletion
					message [length] = NULL_CHAR; // replace deleted character with
					                              // a null character
				}
			}
			else // if its a non-return & non-deletion keystroke
			{
				sendByte (data); // send it character to command line
				message [length] = toUpper (data); // convert it to uppercase and add
				message [length + 1] = NULL_CHAR; //  assign null to end of 'string'
				length++;
			}
		}
	}
	// if they end the input with a enter key, we only need to print a new line
	if (data == CARRIAGE_RETURN)
		newLine ();
	else
		enterLine ();
	// otherwise they went over the character limit, and we need to enter a line

	return;
}

// ignores spaces within a string, and returns index to continue at.
int cutSpacing (char input[], int i)
{
	int j;
	for (j = 0; (!checkAlphaCap(input[i]) && !checkNumeric(input[i])
	              && (input[i] != NULL_CHAR)); j++)
		i++;
		
	return i;
}

// Parses a given input and attempts to seperate into 3 distinct commands
void parseInput (char input[], char cmd1[], char cmd2[],
                 char cmd3[], char expectEnd[])
{
	// null out values to avoid garbage
	int i = 0;
	int j;
	
	cmd1 [0] = NULL_CHAR;
	cmd2 [0] = NULL_CHAR;
	cmd3 [0] = NULL_CHAR;
	i = cutSpacing (input, i);
	// loop through and append to first command, stop if null or white space
	for (j = 0; (input[i] != NULL_CHAR) && (input[i] != ' '); j++)
	{
		cmd1 [j] = input[i];
		cmd1 [j + 1] = NULL_CHAR;
		i++;
	}
	// check if there is excessive spacing, and set index accordingly
	if (input[i] != NULL_CHAR)
		i = cutSpacing(input, i);
	// continue looping to obtain second command.
	for (j = 0; (input[i] != NULL_CHAR) && (input[i] != ' '); j++)
	{
		cmd2 [j] = input[i];
		cmd2 [j + 1] = NULL_CHAR;
		i++;
	}
	// check for excessive spacing again
	if (input[i] != NULL_CHAR)
		i = cutSpacing(input, i);
		// continue looping to obtain second command.

	for (j = 0; (input[i] != NULL_CHAR) && (input[i] != ' '); j++)
	{
		cmd3 [j] = input[i];
		cmd3 [j + 1] = NULL_CHAR;
		i++;
	}
	// Set the character following to expectEnd, should be null.
	// check for excessive spacing again
	if (input[i] != NULL_CHAR)
		i = cutSpacing(input, i);
	expectEnd[0] = input[i];
	
	return;
}

// Once input has been parsed and seperated, this function will 
// preform any needed commands.
void handleInput (char* command, char* ledNum, char* subCommand, 
	                char* expectEnd, uint32_t avgTime[],
									uint32_t timeNullAvg) 
{
	// make sure we have the right commands
	if (!checkCommand (command, ledNum, subCommand, expectEnd))
	{
		// if nothing is entered, simply return to start
		if (charArrayCompare(command, "")) 
			return;
		// if not, send an error message
		printWithTab (ERROR);
		enterLine ();
		return;
	}
	// print help message if HELP
	if (charArrayCompare(command, "HELP"))
		printHelp ();
	// go into a function to run timing tests.
	else if (charArrayCompare(command, "TIMING"))
	{
		timeNullAvg = beginTiming (avgTime, timeNullAvg);
		printTiming (avgTime, timeNullAvg);
		enterLine ();
		return;
	}
	// print date & time compiles if COMPILED
	else if (charArrayCompare(command, "COMPILED"))
		printCompiled ();
	
	else if (charArrayCompare(command, "MRBLUESKY"))
	{
		printBlueSky ();
	}
	
	else if (charArrayCompare (command, "LED"))
	{
		// Check if they want to query an LED
		if (charArrayCompare (subCommand, "QUERY"))
		{
			printQuery (ledNum);
		}
		else if (charArrayCompare (subCommand, "BLINK"))
		{
			ledInterupt = ledNum;
			timer_init_with_interrupts ();
		}
		// Check if they want to turn on an LED
		else if (charArrayCompare (subCommand, "ON"))
		{
			// Do they want to turn on 1 or all?
			if (charArrayCompare (ledNum , "ALL"))
			{
				allLedOn ();
				printWithTab (ALL_ON_MESSAGE);
			}
			else
			{
				ledOn (strToInt (ledNum));
				printWithTab (ON_MESSAGE);
			}
		}
		// Check if they want to turn off an LED
		else if (charArrayCompare (subCommand, "OFF"))
		{
			// Do they want to turn off 1 or all?
			if (charArrayCompare (ledNum , "ALL"))
			{
				allLedOff ();
				printWithTab (ALL_OFF_MESSAGE);
			}
			else
			{
				ledOff (strToInt (ledNum));
				printWithTab (OFF_MESSAGE);
			}
			
		}
	}
	enterLine ();
}

// convert a given lower case character to upper case
char toUpper (char convertMe) 
{
	if (checkAlphaLow(convertMe))
	{
		return (convertMe - ALPHA_CAP_SHIFT);	
	}
	return convertMe;
}

// return the length of a 'string'
int strLen (const char* message)
{
	int i;
	//empty forloop to sum the value of length
	for(i = 0; message[i] != NULL_CHAR; i++) {}
	return i;
}

// converts a given 'string' numeric to a unsigned numeric
unsigned strToInt (const char* string)
{
	int i;
	int result = 0;
	
	for (i = 0; string[i] != NULL_CHAR; i++)
	{
		if (!checkNumeric(string[i]))
			return 0;
		else
			result = (result * 10) + (string[i] - 48);
	}
	return result;
}

// checks to see if command entered is valid
unsigned checkCommand (char* command, char* ledNum, 
	                     char* subCommand, char* expectEnd)
{
	unsigned commandCheck = 0;
	
	// compare to HELP
	if (charArrayCompare (command, "HELP")) 
		commandCheck = 1;
	
	// compare to TIMING
	if (charArrayCompare (command, "TIMING"))
		commandCheck = 1;
	
	if (charArrayCompare (command, "MRBLUESKY"))
		commandCheck = 1;
	
	// coompare to COMPILED
	if (charArrayCompare (command, "COMPILED")) 
		commandCheck = 1;
	
	// compare to LED
	if (charArrayCompare (command, "LED") && checkSubCommand(subCommand, ledNum))
		commandCheck = 1;
	// make sure expectEnd was indeed null.
	if (expectEnd[0] != 0)
		commandCheck = 0;
	
	return commandCheck;
}

// Makes sure our subcommand is correct
unsigned checkSubCommand (char* subCommand, char* ledNum)
{
	unsigned commandCheck = 0;
	
	// compares to ON
	if (charArrayCompare (subCommand, "ON") &&
		(checkLedNum (ledNum) || charArrayCompare (ledNum, "ALL")))
		commandCheck = 1;
	
	// compares to OFF
	if (charArrayCompare (subCommand, "OFF") &&
		(checkLedNum (ledNum) || charArrayCompare (ledNum, "ALL")))
		commandCheck = 1;
	
	// compares to QUERY
	if (charArrayCompare (subCommand, "QUERY") && checkLedNum(ledNum))
		commandCheck = 1;
	
	return commandCheck;
}

// Makes sure that the LED number provide is in the valid range
unsigned checkLedNum (char* ledNum)
{
	unsigned commandCheck = 0;
	
	if (charArrayCompare (ledNum, "8"))
		commandCheck = 1;
	
	if (charArrayCompare (ledNum, "9"))
		commandCheck = 1;
	
	if (charArrayCompare (ledNum, "10"))
		commandCheck = 1;
	
	if (charArrayCompare (ledNum, "11"))
		commandCheck = 1;
	
	if (charArrayCompare (ledNum, "12"))
		commandCheck = 1;
	
	if (charArrayCompare (ledNum, "13"))
		commandCheck = 1;
	
	if (charArrayCompare (ledNum, "14"))
		commandCheck = 1;
	
	if (charArrayCompare (ledNum, "15"))
		commandCheck = 1;
	
	return commandCheck;
}

// compares two character arrays to see if they are indeed equal
unsigned charArrayCompare (char* str1, char* str2)
{
	int i;
	
	for (i = 0; (i < MAX_USER_INPUT); i++)
	{
		if (str1[i] != str2[i])
			return 0;
		
		if ((str1[i] == NULL_CHAR) || (str2[i] == NULL_CHAR))
			return 1;
	}
	
	return 0;
}

// checks if the character given is within the upper case alpha range
unsigned checkAlphaCap(char input)
{
	if ((input >= ALPHA_LOWER_LIMIT) && (input <= ALPHA_MIDDLE_LIMIT))
		return 1;
	return 0;
}

// checks if the character given is within the lower case alpha range
unsigned checkAlphaLow(char input)
{
	if ((input >= ALPHA_MIDDLE_LIMIT) && (input <= ALPHA_UPPER_LIMIT))
		return 1;
	return 0;
}

// checks if the character given is within the numeric value range
unsigned checkNumeric(char input)
{
	if ((input >= NUMERIC_LOWER_LIMIT) && (input <= NUMERIC_UPPER_LIMIT))
		return 1;
	return 0;
}

// This function exists to take any given uint16 from our timing functions
// and then print it as a character on our CLI.
void printInt (uint16_t input)
{
	int i;
	int digit;
	int print = 0;
	char ascii;
	// the constant is the maximum # of digits a uint16 can have
	for (i = MAGNITUDE_OF_UINT16; i >= 0; i--)
	{
		// We single out the MSB in question
		digit = (input / power (10, i));
		input %= power (10, i);
		// Then check to see if it should be printed
		if ((print != 0) || (digit != 0))
		{
			print = 1;
			ascii = digit + NUMERIC_LOWER_LIMIT;
			sendByte (ascii);
		}
	}
	return;
}

// This functions sole purpose is to return the value of base^exp
int power (int base, int exp)
{
	int result = 1;
	int i;
	
	for (i = 0; i < exp; i++)
	{
		result *= base;
	}
	
	return result;
}
