/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: timer.h
Date Started: October 12, 2017 
Date Finished: October x, 2017 

Purpose: 
*/

void timer_init(void);

int16_t timer_start(void);

int16_t timer_stop(int16_t start_time);

void timer_shutdown(void);