/**
Name: Arsalan Khan
SID: 200343820
Date Created: October 03, 2017
Purpose: The purpose of this file is to simply provide function declartions so that all the functions
		 fleshed out in the comp.cpp file will have a reference.
*/

struct Complex
{
	float real, imaginary;
};

const int NUM_PARAMS = 5;
const int DEG_CONVERSION = 180;
const double PI = 3.14;

Complex compleAddition(Complex, Complex);
Complex complexSubtract(Complex, Complex);
Complex complexMultiply(Complex, Complex);
Complex complexDivide(Complex, Complex);
float complexAbs(Complex);
float complexArg(Complex);
float complexArgDeg(Complex);
Complex complexExp(Complex);
Complex complexInv(Complex);
void printValue(bool, bool, Complex, float);