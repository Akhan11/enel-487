/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 &
File Name: register.h
Date Started: September 28, 2017 
Date Finished: October 5, 2017

Purpose: The purpose of registers.h is to declare values that relate to
         address values for each STM peripheral we use. Having these 
				 defintions makes it much easier to refer to our desired peripheral
				 without having to look up its register value every time.

All values indicated below were pulled from the STM reference manual rev 16.
*/
#pragma once
#include <stdint.h>
#include "stdlib.h"


#define PERIPH_BASE              ((uint32_t)0x40000000)

#define AHBPERIPH_BASE           (PERIPH_BASE + 0x20000)
#define APB1PERIPH_BASE          (PERIPH_BASE)
#define RCC_BASE                 (AHBPERIPH_BASE + 0x1000)
#define APB2PERIPH_BASE          (PERIPH_BASE + 0x10000)
#define GPIOB_BASE               (APB2PERIPH_BASE + 0x0C00)
#define GPIOA_BASE               (APB2PERIPH_BASE + 0x0800)
#define USART2_BASE              (APB1PERIPH_BASE + 0x4400)
#define TIM2_BASE                (APB1PERIPH_BASE + 0x0000)
#define TIM3_BASE                (APB1PERIPH_BASE + 0x0400)
#define TIM4_BASE                (APB1PERIPH_BASE + 0x0800)
#define NVIC_BASE                ((uint32_t)0xE000E100) 

#define RCC_APB1ENR              (* (uint32_t volatile *) (RCC_BASE + 0x1C))
#define RCC_APB2ENR              (* (uint32_t volatile *) (RCC_BASE + 0x18))
#define GPIOB_CRL                (* (uint32_t volatile *) (GPIOB_BASE + 0x00))
#define GPIOB_CRH                (* (uint32_t volatile *) (GPIOB_BASE + 0x04))
#define GPIOB_ODR                (* (uint32_t volatile *) (GPIOB_BASE + 0x0C))
#define GPIOB_BSRR               (* (uint32_t volatile *) (GPIOB_BASE + 0x10))
#define GPIOB_BRR                (* (uint32_t volatile *) (GPIOB_BASE + 0x14))

#define GPIOA_CRL                (* (uint32_t volatile *) (GPIOA_BASE))
#define USART2_BRR               (* (uint32_t volatile *) (USART2_BASE + 0x08))
#define USART2_CR1               (* (uint32_t volatile *) (USART2_BASE + 0x0C))
#define USART2_CR2               (* (uint32_t volatile *) (USART2_BASE + 0x10))
#define USART2_CR3               (* (uint32_t volatile *) (USART2_BASE + 0x14))
#define USART2_SR                (* (uint32_t volatile *) (USART2_BASE + 0x00))
#define USART2_DR                (* (uint32_t volatile *) (USART2_BASE + 0x04))

#define TIM2_CR1                 (* (uint32_t volatile *) (TIM2_BASE))
#define TIM2_CNT                 (* (uint32_t volatile *) (TIM2_BASE + 0x24))
#define TIM2_ARR                 (* (uint32_t volatile *) (TIM2_BASE + 0x2C))

#define TIM3_CR1                 (* (uint32_t volatile *) (TIM3_BASE))
#define TIM3_CNT                 (* (uint32_t volatile *) (TIM3_BASE + 0x24))
#define TIM3_ARR                 (* (uint32_t volatile *) (TIM3_BASE + 0x2C))
#define TIM3_DIER                (* (uint32_t volatile *) (TIM3_BASE + 0x0C))
#define TIM3_PSC                 (* (uint32_t volatile *) (TIM3_BASE + 0x28))
#define TIM3_SR                  (* (uint32_t volatile *) (TIM3_BASE + 0x10))
	
#define TIM4_CR1                 (* (uint32_t volatile *) (TIM4_BASE + 0x00))
#define TIM4_CR2                 (* (uint32_t volatile *) (TIM4_BASE + 0x04))
#define TIM4_SMCR                (* (uint32_t volatile *) (TIM4_BASE + 0x08))
#define TIM4_CCMR1               (* (uint32_t volatile *) (TIM4_BASE + 0x18))
#define TIM4_CCMR2               (* (uint32_t volatile *) (TIM4_BASE + 0x1C))
#define TIM4_CCER                (* (uint32_t volatile *) (TIM4_BASE + 0x20))
#define TIM4_PSC                 (* (uint32_t volatile *) (TIM4_BASE + 0x28))
#define TIM4_ARR                 (* (uint32_t volatile *) (TIM4_BASE + 0x2C))
#define TIM4_CCR1                (* (uint32_t volatile *) (TIM4_BASE + 0x34))
#define TIM4_CCR2                (* (uint32_t volatile *) (TIM4_BASE + 0x38))
#define TIM4_CCR3                (* (uint32_t volatile *) (TIM4_BASE + 0x3C))
#define TIM4_CCR4                (* (uint32_t volatile *) (TIM4_BASE + 0x40))

#define NVIC_ISER_0              (* (uint32_t volatile *) (NVIC_BASE))
#define NVIC_ISPR_0              (* (uint32_t volatile *) (NVIC_BASE + 0x100))
#define NVIC_ICPR_0              (* (uint32_t volatile *) (NVIC_BASE + 0x180))
	

