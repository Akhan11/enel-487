/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: servo.h
Date Started: October 19, 2017 
Date Finished: October , 2017 

Purpose: This file contains function declarations for pwm control of a servo
         motor.
*/
#include "timer.h"

#define RCC_APB2ENR_GPIOB        0x00000008
#define GPIOB_CRL_PB7_AFIO       0xB0000000
#define GPIOB_CRL_PB7_MASK       0xF0000000
#define PERCENT_FACTOR           9
#define MAX_PERCENT              100
#define DEGREE_FACTOR            5
#define MAX_DEGREES              180

/**
Name: servo_init
Input: void
Output: void
Purpose: Configures registers to set up PWM for a servo using TIM4_CH2
*/
void servo_init (void);

/**
Name: servo_set_raw
Input: uint32_t
Output: void
Purpose: Sets the number of PWM ticks driving the servo
*/
void servoSetRaw (uint32_t);

/**
Name: servo_set_percent
Input: uint32_t
Output: void
Purpose: Sets the percent rotation of the servo
*/
void servoSetPercent (uint32_t);

/**
Name: servo_set_degrees
Input: uint32_t
Output: void
Purpose: Sets the degrees rotation of the servo
*/
void servoSetDegrees (uint32_t);


