/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 &
File Name: CLI.h
Date Started: October 9, 2017 
Date Finished: October 12, 2017 

Purpose: The purpose of serial.h is to hold function headers/declerations
         for all things related to our CLI. This includes everything from
				 recieving user input, parsing input, and calling functions to execute
				 valid commands given by the user. In addition to functions that help
				 us achieve the aforementioned items we have list of defines, and
				 'string' declerations to make everything in the .c file much cleaner.
				 
				 All define values are related to different ascii values, each name
				 is fairly descriptive. As an example we will take a look at
				 
				 ALPHA_LOWER_LIMT   -> 0x41, first capital letter on ascii table
				 ALPHA_MIDDLE_LIMIT -> 0x61, first lower case letter
				 ALPHA_UPPER_LIMIT  -> 0x7A, last lower case letter
				 
				 You can find the ascii table we utilized by navigating to the 
				 page listed below.
				 
				 http://www.asciitable.com/
*/
#pragma once

#include "serial.h"

/**
The block of defines below exist so we do not use magic numbers in our code.
All values except MAX_USER_INPUT are obtained from the offical ascii table.
You can refer to the table by going to 
http://www.asciitable.com/ 

MAX_USER_INPUT was set to 20, because we wanted to limit how much the user
should be able to enter as a command. Since all our valid commands are under
10 characters in length, it makes no sense to allow an infinte length as an 
input.
*/
#define MAX_USER_INPUT           20
#define MAX_LINE_LIMIT           80
#define ALPHA_LOWER_LIMIT        0x41
#define ALPHA_MIDDLE_LIMIT       0x61
#define ALPHA_UPPER_LIMIT        0x7A
#define ALPHA_CAP_SHIFT          0x20
#define NUMERIC_LOWER_LIMIT      0x30
#define NUMERIC_UPPER_LIMIT      0x39
#define BACKSPACE                0x8
#define DELETE                   0x7F
#define CARRIAGE_RETURN          0xD
#define NEW_LINE                 0xA
#define NULL_CHAR                0x0

/**
below is a block of declerations for strings to be used by our CLI.
These messages range from our names, to information on how to input
correct commands.
*/
// Initial message group
const static char* NAMES = "Akaiah Shell [Version 2.3]\n";
const static char* COPY = "Copyright <c> 2017 Akaiah. All rights reserved.\n";
const static char* INSTRUCTIONS = "Below is a list of valid Instructions:\n";
const static char* HELP = "help: Will provide a list of valid entries.\n";
const static char* SMALL_HELP = "Type 'help' for a list of commands\n";
const static char* TAB = "  ";
const static char SPACE = ' ';

// LED on related messages
const static char* LED_ON = "led #led on: This will turn on the "
														"indicated LED\n";
const static char* ALL_LED_ON = "led ALL on: This will turn on all LEDs.\n";
const static char* ON_MESSAGE = "LED turned on.\n\r";
const static char* ALL_ON_MESSAGE = "All LEDs turned on.\n\r";


// LED off related messages
const static char* LED_OFF = "led #led off: This will turn off the "
															"indicated LED.\n";
const static char* ALL_LED_OFF = "led ALL off: This will turn off all LEDs.\n";
const static char* OFF_MESSAGE = "LED turned off.\n\r";
const static char* ALL_OFF_MESSAGE = "All LEDs turned off.\n\r";


// led Query related messages
const static char* LED_QUERY = "led #led query: This will tell you the "
                               "state of the indicated LED.\n";

// Date/Time related messages
const static char* DATE_TIME =  "compiled: This will provide the time "
                                "which the program was compiled.\n";

const static char* DATE_TIME_MESSAGE = "The exact time the program "
                                        "was compiled and flashed was: ";

// misc messages
const static char* ERROR = "The command entered was not valid expression\n";
const static char* ENTER = "peace:>";

/**
Name: combineStrings 
Input: two const char*
Output: one const char* 
Purpose: The purpose of this function is to add two given char*, and return
         a single combined char*. This is because we can't just preform
				 char* + char*.
*/
const char* combineStrings (const char*, const char*);

/**
Name: newLine 
Input: None
Output: None
Purpose: The purpose of this function is to print a '\n' character to the CLI.
         This character will simply move the cursor down vertial once.
*/
void newLine (void);
/**
Name: returnLine 
Input: None
Output: None
Purpose: The purpose of this function is to print a '\r' character to the CLI.
         This character will simply move the cursor to the start of the line.
*/
void returnLine (void);
/**
Name: enterLine 
Input: None
Output: None
Purpose: The purpose of this function is to print a '\n' & '\r' character to
         the CLI. This will mimic someone press the enter button in a text
				 editor.
*/
void enterLine (void);

/**
Name: welcomeMessage 
Input: None
Output: None
Purpose: The purpose of this function is to take existing char* values and 
         print them to the screen to create a welcome message for our user
				 to read.
*/
void welcomeMessage (void);
/**
Name: printString 
Input: One char *
Output: None
Purpose: The purpose of this function is to print a given char*. This is
         accomplished by going through the char* one character at a time
				 and calling a different function to print a single character.
*/
void printString (const char*);
/**
Name: printWithTab 
Input: One char *
Output: None
Purpose: The purpose of this function is to print a given char* with a two
         space tab infront of it. This is accomplished by going through the
				 char* one character at a time and calling a different function to
				 print a single character.
*/
void printWithTab (const char*);
/**
Name: printChar 
Input: One char
Output: None
Purpose: The purpose of this function is to send a byte to the command line
         which is equal to the char passed in.
*/
void printChar (char);
/**
Name: printHelp 
Input: None
Output: None
Purpose: The purpose of this function is to print pre-existing char* to the
         command line. These char* contain information on what is counted as
				 a valid command for our system.
*/
void printHelp (void);
/**
Name: printCompiled 
Input: None
Output: None
Purpose: The purpose of this function is to print to the command line
         information about when this program was last compiled. It will
				 display the date and then time (24 hour format).
*/
void printCompiled (void);
/**
Name: printQuery 
Input: One char*
Output: None
Purpose: The purpose of this function is to print wheter or not a given
         LED is on/off. We determine what LED to check through the char*
				 passed in as an input.
*/
void printQuery (char*);

/**
Name: recieveInput 
Input: One char[]
Output: none 
Purpose: The purpose of this function is to recieve input from the user
         through the command line. It will do so with a forloop that preforms
				 multiple checks. 
*/
void recieveInput (char []);
/**
Name: cutSpacing 
Input: One char[], and one integer
Output: integer
Purpose: In the case where there is multiple white spaces between valid 
         commands this function will effectivly 'eliminate' them. It will
				 do so by passing in the array, and the value of i where the first
				 whtie space appears. It will keep going through the string until 
				 a non-white space character is found, then it will simply return
				 that index so the regular function operates as normal.
*/
int cutSpacing (char[], int);
/**
Name: parseInput 
Input: Five char[]
Output: None 
Purpose: This function will take a string which contains one full statement
         and pull out the information we want. This function will retain values
				 for "command", "subCommnad", and "ledValue". These are vital for 
				 preforming any command.
*/
void parseInput (char [], char [], char [], char [], char []);
/**
Name: handleInput 
Input: Four char*
Output: None
Purpose: This function will take the previously parsed information, and decide
         if the command entered is:
				 1) Valid
				 2) Within the range
				 3) Exactly 3 values
				 4) What function it is calling
				 
				 Finally it will call the needed functions.
*/
void handleInput (char*, char*, char*, char*);
/**
Name: toUpper 
Input: One char
Output: One char
Purpose: This function will take a lower case character value, and 
         convert it to a uppercase.
*/
char toUpper (char);
/**
Name: strLen 
Input: One const char*
Output: One integer 
Purpose: This function runs a simple forloop to count the number of characters
         in a given char*. It then returns that number
*/
int strLen (const char*);
/**
Name: strToInt 
Input: const char*
Output: unsigned 
Purpose: This function will take our parsed const char* of the LED value
         and convert it to a numeric unsigned value.
*/
unsigned strToInt (const char*);

/**
Name: checkCommand 
Input: Four char*
Output: Unsinged
Purpose: Since we do not have access to bools, we will use an unsigned
         function that returns 1 or 0 depending on failure/success. This
				 function will check if our inital command is one of:
				 1) LED
				 2) HELP
				 3) COMPILED
				 
				 In addition it will also check to be sure there are no extra values
				 entered as well.
*/
unsigned checkCommand (char*, char*, char*, char*);
/**
Name: checkSubCommand 
Input: Two char*
Output: Unsigned
Purpose: This function will check if our sub command is one of:
				 1) ON
				 2) OFF
				 3) QUERY
*/
unsigned checkSubCommand (char*, char*);
/**
Name: checkLedNum 
Input: One char*
Output: Unsigned
Purpose: This function will check if the LED number provided is within our
				 acceptable range.
*/
unsigned checkLedNum (char*);
/**
Name: charArrayCompare 
Input: Two char*
Output: Unsigned
Purpose: This function will comape and check if two char* are the same.
*/
unsigned charArrayCompare (char*, char*);
/**
Name: checkAlphaCap 
Input: One char
Output: Unsigned
Purpose: This function will check if the given character is a upper case 
         alpha.
*/
unsigned checkAlphaCap (char);
/**
Name: checkAlphaCap 
Input: One char
Output: Unsigned
Purpose: This function will check if the given character is a lower case 
         alpha.
*/
unsigned checkAlphaLow (char);
/**
Name: checkAlphaCap 
Input: One char
Output: Unsigned
Purpose: This function will check if the given character is a numeric value
*/
unsigned checkNumeric (char);
