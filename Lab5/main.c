/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 &
File Name: main.c
Date Started: September 28, 2017 
Date Finished: October 12, 2017

Purpose: The purpose of this file is to make function calls that will allow
         our CLI interface to run seamlessly. Main itself will only be
				 responsible for declaring intial values, so that data may be
				 stored in them at a later point. 
				 
				 At a very high level our CLI interface works in three steps:
				 
				 1) Recieve input from the user
				 2) Parse/Analyze the recieved string into useable values
				 3) Take the cleaned up string, and preform a task if a valid
				    expression has been entered. Otherwise display error message
	
         For a more elaborate explination of allowed inputs, how the input
				 is parsed, and what valid expressions are please check the README.
				 Another option is to check the function decleration in its respective
				 .h file, there you will find a detailed explination of everything to
				 do with that function.
*/
#include "serial.h"
#include "ledControl.h"
#include "CLI.h"
#include "timer.h"
#include "servo.h"
#include "ADC.h"

uint16_t global_adc_1_value;
unsigned global_adc_new_value = 0;


int main()
{	
	char userInput [MAX_USER_INPUT];
	char command [MAX_USER_INPUT];
	char subCommand [MAX_USER_INPUT];
	char ledNum [MAX_USER_INPUT];
	char expectEnd [1] ;
	uint32_t avgTime[NUM_OF_TESTS] = {0};
	uint32_t timeNullAvg = 0;
	
	openUsart();
	ledInit ();
	timer_init ();
  //timer_init_with_interrupts ();
	servo_init ();
	adcInit ();
	
	welcomeMessage();	
	while (1)
	{
		recieveInput (userInput);
		returnLine ();
		parseInput (userInput, command, ledNum, subCommand, expectEnd);
		handleInput (command, ledNum, subCommand, expectEnd, avgTime,
		             timeNullAvg);
	}
	
}

// This is our interrupt handlers code execution
// It simply queries the state of an LED, and turns it on/off accordingly
void TIM3_IRQHandler ()
{
	if (queryLed (LED_8))
		ledOff (LED_8);
	else
		ledOn (LED_8);
	
	TIM3_SR = 0;
}

// When our interrupt is triggered for our ADC, we read a value, and set a flag
// indicating that a new value has been obtained
void ADC1_2_IRQHandler ()
{
	global_adc_1_value = adcReadValue ();
	global_adc_new_value = 1;
}

