/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: timer_impl.cpp
Date Started: October 12, 2017 
Date Finished: October 18, 2017 

Purpose: The purpose of this file is to hold all functions related to the
         implementation of our timer TIM2, TIM3 & TIM4. TIM3 is used as an
				 interrupt to drive an LED, while TIM2 is used as a timing mechanism
				 for determining clock cycles required for different operations.
				 TIM4 is used for PWM timing to control a servo motor.
*/
#include "timer.h"

// This function exists to intialize our timer for user
void timer_init(void)
{
	// Enable the timer we would like to use
	RCC_APB1ENR |= TIM2_EN; 
	// Set the counting direction for our timer & enable counting
	TIM2_CR1 |= TIM2_CR1_DIR;
	TIM2_ARR = TIM2_ARR_MAX;
	TIM2_CR1 |= TIM2_CR1_CEN;
	
	return;
}

void timer_init_with_interrupts (void)
{
	// Enable clock for TIM3
	RCC_APB1ENR |= TIM3_EN;
	
	// Set up TIM3 as a down counter starting at 500 that will fire an
	// interrupt on reload.  Set frequency to 10 kHz (using prescalar of 7200)
	TIM3_CR1 |= TIM2_CR1_DIR;
	TIM3_ARR = TIM2_ARR_500;
	TIM3_PSC = TIM2_PSC_7K2;
	TIM3_DIER |= TIM2_DIER_UPDT;
	
	// Enable interrupts from TIM3 in the NVIC
	NVIC_ISER_0 |= NVIC_BIT_29;
	
	// Start the timer
	TIM3_CR1 |= TIM2_CR1_CEN;
	
	return;
}

// Configures TIM4 channel 2 as a pwm output
void pwm_init (void)
{
	// Enable clock for TIM4
	RCC_APB1ENR |= TIM4_EN;
	
	// Reset control registers
	TIM4_CR1 = 0;
  TIM4_CR2 = 0;
	
	TIM4_PSC = TIM4_PSC_500KHZ; 
  TIM4_ARR = TIM4_ARR_10K;
	
	TIM4_CCR2  = MIN_TICKS;
	TIM4_CCMR1 = TIM4_CCMR_CH2_PWM;
	TIM4_CCER  = TIM4_CCER_CH2_OUT;
	TIM4_SMCR  = 0; // Reset slave mode config
	
	TIM4_CR1 |= TIM4_CR1_URS;
	
	// Enable timer
	TIM4_CR1 |= TIM4_CR1_CEN;
	
	return;
}

void pwmCCRSet (uint16_t ticks)
{
	if ((ticks >= MIN_TICKS) && (ticks <= MAX_TICKS))
		TIM4_CCR2 = ticks;
	
	return;
}

// Retrieves the current value of our timer
int16_t timer_start(void)
{
	return TIM2_CNT;
}

// Returns the difference of our present timer value - initial timer value
int16_t timer_stop (int16_t startSigned)
{
	uint16_t end;
	uint16_t start;
	uint16_t diff;
	
	end = TIM2_CNT;
	
	start = intToUint (startSigned);
	
	if (end >= start)
		diff = (start + TWO_POW_16) - end;
	else
		diff = start - end;
	
	return diff;
}

// Shuts the timer down
void timer_shutdown(void)
{
	TIM2_CR1 &= ~TIM2_CR1_CEN;
	
	TIM2_ARR = 0x0000;
	TIM2_CR1 &= ~TIM2_CR1_DIR;
	
	RCC_APB1ENR &= ~TIM2_EN;

	return;
}

// Converts a given integer to a Uint, this makes it easier for us to
// read and print our values
uint32_t intToUint (int16_t signedInt)
{
	uint32_t unsignedInt;

	if (0x8000 == (signedInt & BIT15))
		unsignedInt = TWO_POW_16 + signedInt;
	else
		unsignedInt = signedInt;
	
	return unsignedInt;
}


