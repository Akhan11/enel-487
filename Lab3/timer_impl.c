/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: timer_impl.cpp
Date Started: October 12, 2017 
Date Finished: October 18, 2017 

Purpose: The purpose of this file is to hold all functions related to the
         implementation of our timer TIM2 & TIM3. TIM3 is used as an interrupt
				 to drive an LED, while TIM2 is used as a timing mechanism for
				 determining clock cycles required for different operations. 
*/
#include "timer.h"

// This function exists to intialize our timer for user
void timer_init(void)
{
	// Enable the timer we would like to use
	RCC_APB1ENR |= TIM2_EN; 
	// Set the counting direction for our timer & enable counting
	TIM2_CR1 |= TIM2_CR1_DIR;
	TIM2_ARR = TIM2_ARR_MAX;
	TIM2_CR1 |= TIM2_CR1_CEN;
	
	return;
}

void timer_init_with_interrupts (void)
{
	// Enable clock for TIM3
	RCC_APB1ENR |= TIM3_EN;
	
	// Set up TIM3 as a down counter starting at 10,000 that will fire an
	// interrupt on reload.  Set frequency to 10 kHz (using prescalar of 7200)
	TIM3_CR1 |= TIM2_CR1_DIR;
	TIM3_ARR = TIM2_ARR_10K;
	TIM3_PSC = TIM2_PSC_7K2;
	TIM3_DIER |= TIM2_DIER_UPDT;
	
	// Enable interrupts from TIM3 in the NVIC
	NVIC_ISER_0 |= NVIC_BIT_29;
	
	// Start the timer
	TIM3_CR1 |= TIM2_CR1_CEN;
	
	return;
}

// Retrieves the current value of our timer
int16_t timer_start(void)
{
	return TIM2_CNT;
}

// Returns the difference of our present timer value - initial timer value
int16_t timer_stop (int16_t startSigned)
{
	uint16_t end;
	uint16_t start;
	uint16_t diff;
	
	end = TIM2_CNT;
	
	start = intToUint (startSigned);
	
	if (end >= start)
		diff = (start + TWO_POW_16) - end;
	else
		diff = start - end;
	
	return diff;
}

// Shuts the timer down
void timer_shutdown(void)
{
	TIM2_CR1 &= ~TIM2_CR1_CEN;
	
	TIM2_ARR = 0x0000;
	TIM2_CR1 &= ~TIM2_CR1_DIR;
	
	RCC_APB1ENR &= ~TIM2_EN;

	return;
}

// Converts a given integer to a Uint, this makes it easier for us to
// read and print our values
uint32_t intToUint (int16_t signedInt)
{
	uint32_t unsignedInt;

	if (0x8000 == (signedInt & BIT15))
		unsignedInt = TWO_POW_16 + signedInt;
	else
		unsignedInt = signedInt;
	
	return unsignedInt;
}


