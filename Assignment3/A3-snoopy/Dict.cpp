#include "Dict.h"

void randomWordGet(uint32_t index)
{
	//Mod our random number by max allowed characters to asure we are in the
	//correct range
	index %= MAXIMUM_ALLOWABLE_CHARACTERS;
	//This will itterate through to make sure we are at the start of a word
	while (dicked[index] != '\n')
	{
		index++;
	}
	//loop for printing current word
	while (dicked[index+1] != '\n')
	{
		//If we happen to be on the last word, go to set the index to 0
		if (dicked[index + 1] == '\0')
			index = 0;
		printf("%c", dicked[index + 1]);
		index++;
	}
	// add a spacer for formatting
	printf("%c", ' ');
}

//assign a new random value to the randNum reference
void randomNumGen(uint32_t *randNum)
{
	*randNum = rand() * rand();
}