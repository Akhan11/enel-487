Measurements were taken by removing the IR sensor and servo section
of the tube assembly and pointing it horizontally.  The beam of the
sensor was aligned with a meter stick and an object (with a piece
of paper white attached to the side) was placed on the meter stick.
The edge was aligned with the distance to be measured.

The main function of the program was temporarily modified so that 
the raw reading from the IR sensor was updated on the display
approximately once per second.

Five readings for each distance were recorded in a spreadsheet and
the average of the readings was taken.

Since the ADC has a resolution of 12 bits and a range from 0V to
3.3V, each bit represents an 806μV change.  To determine the output
voltage, the average reading value was multiplied by 806e-6.

The output voltages were placed in a scatter plot and a trend line
was drawn revealing that the relationship between distance and the
voltage output of the IR sensor is non-linear.