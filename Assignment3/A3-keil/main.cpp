/*
Name: Arsalan Khan
SID: 200343820
Project: Assignment 3
Date: 2017/12/02

Purpose: This program will accept a user input, to use as a seed for our
         random function. It will then use that random value to go to a
				 location in our dictionary. Once there it will read out one word.
				 It will then generate a new random number, and print out the next,
				 until all 5 words have been outputted.
*/
#include "Dict.h"

int main()
{
	// declare a reference to my randomly generated number
	uint32_t randomGenNumber;
	uint32_t *reference = &randomGenNumber;
	
	scanf ("%d", &randomGenNumber);
	//srand(time(0));
	// Seed our random number generator then obtain a random value
	srand(randomGenNumber);
	randomNumGen (reference);
	//Loop to get five words, each time we will generate a new random number
	for (uint32_t i = 0; i < 5; i++)
	{
		randomWordGet(randomGenNumber);
		randomNumGen(reference);
	}
	// print newline for formatting at end
	printf("%c", '\n');
}
