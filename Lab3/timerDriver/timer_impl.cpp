/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: timer_impl.cpp
Date Started: October 12, 2017 
Date Finished: October x, 2017 

Purpose: 
*/

#include "timer_impl.h"

void timer_init(void)
{
	TIM2_CR1 |= TIM2_CR1_DIR;
	TIM2_ARR = TIM2_ARR_MAX;
	
	TIM2_CR1 |= TIM2_CR1_CEN;
	
	return;
}

int16_t timer_start(void)
{
	int16_t value;
	
	value = TIM2_CNT;
	
	return value;
}