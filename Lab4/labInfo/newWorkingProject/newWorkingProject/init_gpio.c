/*----------------------------------------------------------------------------
 STM32 GPIO setup.
 initializes the GPIOx_CRL and GPIOxCRH register
 *----------------------------------------------------------------------------*/
#include "minimal_regs.h"

void stm32_GpioSetup (void) {
  
    RCC_APB2ENR |= 1u<<3;	// bit 3: IOPBEN=1, enable GPIOB clock
    
    GPIOB_CRL = 
        (0xBu<<28) | // bits 31:28, PB7, output mode, 10MHz, alt func open drain
        (3u<<24) | // bits 27:24, PB6, output mode, 50MHz, push-pull
        (3u<<20) | // bits 23:20, PB5, output mode, 50MHz, push-pull
        (3u<<16) | // bits 19:16, PB4, output mode, 50MHz, push-pull
        (3u<<12) | // bits 15:12, PB3, output mode, 50MHz, push-pull
        (3u<< 8) | // bits 11:8,  PB2, output mode, 50MHz, push-pull
        (3u<< 4) | // bits 7:4,   PB1, output mode, 50MHz, push-pull
        (3u<< 0);  // bits 3:0,   PB0, output mode, 50MHz, push-pull

} // end of stm32_GpioSetup

