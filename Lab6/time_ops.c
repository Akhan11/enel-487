/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: timer.h
Date Started: October 12, 2017 
Date Finished: October 18, 2017 

Purpose: The purpose of this file is to hold the C code for anything
         related to our timing operations. This includes generating random
				 numbers, running the tests, and returning/assigning appropriate
				 values
*/
#include "timer.h"

// This function will generate a random 8 byte struct. It will simply
// fill our struct up with 1 64 bit integer -> Which is 1 Byte
Byte_8 randomGenerator_8Byte (Byte_8 structure) 
{
	structure.bits = randomGenerator_64 ();
	return structure;
}

// This function will generate a random 128 byte struct. It will simply
// fill our struct up with 16 64 bit integers -> Which is 128 Bytes
Byte_128 randomGenerator_128Byte (Byte_128 structure)
{
	int i;
	for (i = 0; i < BYTE_8_128; i++)
	{
		structure.bits[i].bits = randomGenerator_64 ();
	}
	return structure;
}

// This function will generate a random 32 bit integer utilizing the rand() 
// function
int32_t randomGenerator_32 ()
{
		return rand ();
}

// This function will generate a random 64 bit integer utilizing the 32 random
// generator and multiplying their values
int64_t randomGenerator_64 ()
{
	return (randomGenerator_32 () * randomGenerator_32 ());
}

// This function will return the time taken add two 32 bit integers
// The process is quite simple. The returned time will be an unsigned integer.
uint16_t add_32 () 
{
	volatile int32_t temp1 = randomGenerator_32();
	volatile int32_t temp2 = randomGenerator_32();
	volatile int16_t timeTaken;
		
	timeTaken = timer_start ();
	temp1 = temp1 + temp2;
	timeTaken = timer_stop (timeTaken);
	
	return (intToUint(timeTaken));
	
}

// This function will return the time taken add two 64 bit integers
// The process is quite simple. The returned time will be an unsigned integer.
uint16_t add_64 ()
{
	volatile int32_t temp1 = randomGenerator_64();
	volatile int32_t temp2 = randomGenerator_64();
	volatile int16_t timeTaken;
	
	timeTaken = timer_start ();
	temp1 = temp1 + temp2;
	timeTaken = timer_stop (timeTaken);
	
	return (intToUint(timeTaken));
}

// This function will return the time taken multiply two 32 bit integers
// The process is quite simple. The returned time will be an unsigned integer.
uint16_t multi_32 ()
{
	volatile int32_t temp1 = randomGenerator_32();
	volatile int32_t temp2 = randomGenerator_32();
	volatile int64_t temp3;
	volatile int16_t timeTaken;
	
	timeTaken = timer_start ();
	temp3 = temp1 * temp2;
	timeTaken = timer_stop (timeTaken);
	
	return (intToUint(timeTaken));
}

// This function will return the time taken multiply two 64 bit integers
// The process is quite simple. The returned time will be an unsigned integer.
uint16_t multi_64 ()
{
	volatile int32_t temp1 = randomGenerator_64();
	volatile int32_t temp2 = randomGenerator_64();
	volatile int64_t temp3;
	volatile int16_t timeTaken;
	
	timeTaken = timer_start ();
	temp3 = temp1 * temp2;
	timeTaken = timer_stop (timeTaken);
	
	return (intToUint(timeTaken));
}

// This function will return the time taken divide two 32 bit integers
// The process is quite simple. The returned time will be an unsigned integer.
uint16_t div_32 ()
{
	volatile int32_t temp1 = randomGenerator_32();
	volatile int32_t temp2 = randomGenerator_32();
	volatile int16_t timeTaken;
	
	timeTaken = timer_start ();
	if (temp2)
		temp1 = temp1 / temp2;
	timeTaken = timer_stop (timeTaken);
	
	return (intToUint(timeTaken));
}


// This function will return the time taken divide two 64 bit integers
// The process is quite simple. The returned time will be an unsigned integer.
uint16_t div_64 ()
{
	volatile int32_t temp1 = randomGenerator_64();
	volatile int32_t temp2 = randomGenerator_64();
	volatile int16_t timeTaken;
	
	timeTaken = timer_start ();
	if (temp2)
		temp1 = temp1 / temp2;
	timeTaken = timer_stop (timeTaken);
	
	return (intToUint(timeTaken));	
}


// This function will return the time taken to copy 1 8B -> 1 8B
// struct. The process is quite simple. The returned time will be an
// unsigned integer.
uint16_t struct_8 ()
{
	volatile struct Byte_8 temp1;
	volatile struct Byte_8 temp2;
	volatile int16_t timeTaken;
	
	temp1 = randomGenerator_8Byte (temp1);
	
	timeTaken = timer_start ();
	temp2 = temp1;
	timeTaken = timer_stop (timeTaken);
	
	return (intToUint(timeTaken));
}

// This function will return the time taken to copy 1 128B -> 1 128B
// struct. The process is quite simple. The returned time will be an
// unsigned integer.
uint16_t struct_128 ()
{
	volatile struct Byte_128 temp1;
	volatile struct Byte_128 temp2;
	volatile int16_t timeTaken;
	
	temp1 = randomGenerator_128Byte (temp1);
	
	timeTaken = timer_start ();
	temp2 = temp1;
	timeTaken = timer_stop (timeTaken);
	
	return (intToUint(timeTaken));
}

// This function will return the time required to copy 1 '1024' byte value
// into another. It should be noted that it is not a 1024 -> 1024 copy, 
// instead it is 128B * 8 -> 128B * 8 copy.
uint16_t struct_1024 ()
{
	volatile struct Byte_128 temp1;
	volatile struct Byte_128 temp2;
	int i;
	int16_t timeTemp = 0;
	
	temp1 = randomGenerator_128Byte (temp1);
	timeTemp = timer_start ();
	for (i = 0; i < BYTE_128_1024; i++)
	{
		temp2 = temp1;
	}
	timeTemp = timer_stop (timeTemp);
	
	return (intToUint(timeTemp));
}

// This function simply obtains the time required for use to be able to
// pull a value from the CNT register twice, and returns it as a uint.
uint16_t getNullTime (void)
{
	volatile int16_t timeTaken;

	timeTaken = timer_start ();
	timeTaken = timer_stop (timeTaken);

	return (intToUint(timeTaken));
}

// This function uses nested for loops to determine the average time
// taken for executing an array of different instructions
uint32_t beginTiming (uint32_t avgTime[], uint32_t timeNullAvg)
{
	// initialize and 0
	int i, j;
	uint32_t timeNull = 0;
	// Outer loop indicating the number of tests that need to be executed
	for (i = 0; i < NUM_OF_TESTS; i ++)
	{
		// inner loop indicating the number of trials each test will be
		// executed by
		for (j = 0; j < NUM_OF_TRIALS; j++)
		{
			// Depending on the state of our i variable, we will preform a different
			// test
			switch (i)
			{
				case 0:
					// We will accumulate an average null time to return only once
          timeNullAvg += getNullTime ();
				  // For every other case we will take the time required to execute our
				  // command and then subtract a null time from it
					avgTime[i] += (add_32 () - getNullTime ());
					break;
				case 1:

					avgTime[i] += (add_64 () - getNullTime ());
					break;
				case 2:

					avgTime[i] += (multi_32 () - getNullTime ());
					break;
				case 3:

					avgTime[i] += (multi_64 () - getNullTime ());
				  break;
				case 4:

					avgTime[i] += (div_32 () - getNullTime ());
				  break;
				case 5:

					avgTime[i] += (div_64 () - getNullTime ());
				  break;
				case 6:

					avgTime[i] += (struct_8 () - getNullTime ());
				  break;
				case 7:
					avgTime[i] += (struct_128 () - getNullTime ());
				  break;
				case 8: 
					avgTime[i] += (struct_1024 () - getNullTime ());
				  break;
				default:
					break;
			}
		}
		// Make sure we are obtaining the averages, not the cumulative values
		if (i == 0)
			timeNullAvg /= NUM_OF_TRIALS;
		avgTime [i] /= NUM_OF_TRIALS;
		avgTime [i] -= timeNull;
	}
	return timeNullAvg;
}

