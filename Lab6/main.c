/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 &
File Name: main.c
Date Started: September 28, 2017 
Date Finished: October 12, 2017

Purpose: The purpose of this file is to make function calls that will allow
         our CLI interface to run seamlessly. Main itself will only be
				 responsible for declaring intial values, so that data may be
				 stored in them at a later point. 
				 
				 At a very high level our CLI interface works in three steps:
				 
				 1) Recieve input from the user
				 2) Parse/Analyze the recieved string into useable values
				 3) Take the cleaned up string, and preform a task if a valid
				    expression has been entered. Otherwise display error message
	
         For a more elaborate explination of allowed inputs, how the input
				 is parsed, and what valid expressions are please check the README.
				 Another option is to check the function decleration in its respective
				 .h file, there you will find a detailed explination of everything to
				 do with that function.
*/
#include "serial.h"
#include "ledControl.h"
#include "CLI.h"
#include "timer.h"
#include "servo.h"
#include "ADC.h"
#include "controls.h"

uint16_t global_adc_1_value;
unsigned global_adc_new_value = 0;
unsigned global_servo_update = 0;
unsigned global_ustart_recieved = 0;
uint32_t global_control_set_ball_raw;


int main()
{	
	char userInput [MAX_USER_INPUT] = "\0";
	char command [MAX_USER_INPUT];
	char subCommand [MAX_USER_INPUT];
	char ledNum [MAX_USER_INPUT];
	char expectEnd [1] ;
	int inputLength;
	int i;
	unsigned parsingInput = 0;
	uint32_t avgTime[NUM_OF_TESTS] = {0};
	uint32_t timeNullAvg = 0;
	
	openUsart();
	ledInit ();
	timer_init ();
  timer_init_with_interrupts ();
	servo_init ();
	adcInit ();
	
	welcomeMessage();	
	printString (ENTER);
	while (1)
	{
		if (global_ustart_recieved)
		{
			inputLength = strLen (userInput);
			parsingInput = recieveInput(userInput, inputLength);
			if (parsingInput)
			{
				parseInput (userInput, command, ledNum, subCommand, expectEnd);
				handleInput (command, ledNum, subCommand, expectEnd, avgTime,
										 timeNullAvg);
				
				printString (ENTER);
				for(i = inputLength; i > 0; i--)
				{
					checkDelete (userInput, i, BACKSPACE);
				}
				inputLength = 0;
			}

			global_ustart_recieved = 0;
		}
		else if (global_servo_update) 
		{
			global_servo_update = 0;
			
			updateController ();
				if (queryLed (LED_8))
		ledOff (LED_8);
	else
		ledOn (LED_8);
		}
	}
	
}

// This is our interrupt handlers code execution
// It sets a flag to update the servo position every 50 ms.
void TIM3_IRQHandler ()
{
	global_servo_update = 1;
	
	TIM3_SR = 0;
}

// When our interrupt is triggered for our ADC, we read a value, and set a flag
// indicating that a new value has been obtained
void ADC1_2_IRQHandler ()
{
	global_adc_1_value = adcReadValue ();
	global_adc_new_value = 1;
}

void USART2_IRQHandler ()
{
	if (USART2_SR & USART_SR_RXNE)
	{
		USART2_SR &= ~USART_SR_RXNE;
		global_ustart_recieved = 1;
	}
}
