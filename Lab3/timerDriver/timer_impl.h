/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: timer_impl.h
Date Started: October 12, 2017 
Date Finished: October x, 2017 

Purpose: 
*/

#include "registers.h"

#define TIM2_CR1_DIR      0x0010
#define TIM2_CR1_CEN      0x0001
#define TIM2_ARR_MAX      0xFFFF