/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: ADC.h
Date Started: October 28, 2017 
Date Finished: October , 2017 

Purpose: This file contains function code for the ADC block.

///////////////////////////////////////////////////////////////////////////////
// For detailed function discriptions please refer to ADC.h where the function
// is declared. Below is a name list for the utilized functions.
// This files comments will simply explain the code
///////////////////////////////////////////////////////////////////////////////

Functions: adcInit
           adcStartCapture
					 adcReadValue

*/
#include "ADC.h"

void adcInit (void)
{
	// Enable clock for GPIOC
	RCC_APB2ENR |= RCC_APB2ENR_GPIOC | RCC_APB2ENR_ADC1;
	
	// Configure PC0 to floating input
	GPIOC_CRL |= GPIOC_CRL_PC0_MASK;
	ADC1_SQR3 |= SQ1_MASK;
	ADC1_SMPR1 |= SMPR1_MASK;
	ADC1_CR1 |=  EOCIE_MASK | SCAN_MASK;
	ADC1_CR2 |= ADC_ON | RESET_CAL | EXT_TRIGGER | EXT_TRIG_SEL;
	while (ADC1_CR2 & RESET_CAL){}
	ADC1_CR2 |= CAL;
	while (ADC1_CR2 & CAL){}
	
	// Enable interrupts from ADC 1 & 2 in the NVIC
	NVIC_ISER_0 |= NVIC_BIT_18;
}

// Set start in CR2, and set our value global to 0, indicating we want a new
// value
void adcStartCapture (void)
{
	ADC1_CR2 |= SWSTART_MASK;
}

// Return the value storesd in the DR of the ADC
uint16_t adcReadValue (void)
{
	return (ADC1_DR & DR_MASK);
}
