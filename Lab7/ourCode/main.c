/*
    FreeRTOS V7.2.0 - Copyright (C) 2012 Real Time Engineers Ltd.


    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!
    
    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?                                      *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    
    http://www.FreeRTOS.org - Documentation, training, latest information, 
    license and contact details.
    
    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool.

    Real Time Engineers ltd license FreeRTOS to High Integrity Systems, who sell 
    the code with commercial support, indemnification, and middleware, under 
    the OpenRTOS brand: http://www.OpenRTOS.com.  High Integrity Systems also
    provide a safety engineered and independently SIL3 certified version under 
    the SafeRTOS brand: http://www.SafeRTOS.com.
*/

/*
 * This is a very simple demo that demonstrates task and queue usages only in
 * a simple and minimal FreeRTOS configuration.  Details of other FreeRTOS 
 * features (API functions, tracing features, diagnostic hook functions, memory
 * management, etc.) can be found on the FreeRTOS web site 
 * (http://www.FreeRTOS.org) and in the FreeRTOS book.
 *
 * Details of this demo (what it does, how it should behave, etc.) can be found
 * in the accompanying PDF application note.
 *
*/

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Standard include. */
#include <stdio.h>

/* Priorities at which the tasks are created. */
#define mainQUEUE_RECEIVE_TASK_PRIORITY     ( tskIDLE_PRIORITY + 2 )
#define mainQUEUE_SEND_TASK_PRIORITY        ( tskIDLE_PRIORITY + 1 )
#define mainTASK1_PRIORITY                  ( tskIDLE_PRIORITY + 3 )
#define mainTASK0_PRIORITY                  ( tskIDLE_PRIORITY + 4 )

/* The rate at which data is sent to the queue, specified in milliseconds. */
#define mainQUEUE_SEND_FREQUENCY_MS         ( 10 / portTICK_RATE_MS )

/* The number of items the queue can hold.  This is 1 as the receive task
will remove items as they are added, meaning the send task should always find
the queue empty. */
#define mainQUEUE_LENGTH                    ( 1 )

/* The ITM port is used to direct the printf() output to the serial window in 
the Keil simulator IDE. */
#define mainITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define mainITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))
#define mainDEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define mainTRCENA          0x01000000

/*-----------------------------------------------------------*/

/*
 * The tasks as described in the accompanying PDF application note.
 */

static void controlTask( void *pvParameters );
static void servoControl( void *pvParameters );
static void UITASK( void *pvParameters );


/*
 * Redirects the printf() output to the serial window in the Keil simulator
 * IDE.
 */
int fputc( int iChar, FILE *pxNotUsed );

/*-----------------------------------------------------------*/

/* The queue used by both tasks. */
static xQueueHandle cliRX = NULL;
static xQueueHandle ballSet = NULL;
static xQueueHandle adcValueQ = NULL;
static xQueueHandle pwmQ = NULL;




/* One array position is used for each task created by this demo.  The 
variables in this array are set and cleared by the trace macros within
FreeRTOS, and displayed on the logic analyzer window within the Keil IDE -
the result of which being that the logic analyzer shows which task is
running when. */
unsigned long ulTaskNumber[ configEXPECTED_NO_RUNNING_TASKS ];

/*-----------------------------------------------------------*/

#include "serial.h"
#include "ledControl.h"
#include "CLI.h"
#include "ADC.h"
#include "controls.h"
#include "servo.h"



int main(void)
{
    // Copied from blinkOneLight project
    // ----------------------------------------------------------------
    ledInit ();
		openUsart();
		adcInit ();
		servo_init();

		welcomeMessage();	
		printString (ENTER);
    /* Creating a queue for CLI, BallSetHeight, ADC queue, and pwm queue */
    cliRX = xQueueCreate( mainQUEUE_LENGTH, sizeof (char));
		ballSet = xQueueCreate( mainQUEUE_LENGTH, sizeof (int));
		adcValueQ = xQueueCreate( mainQUEUE_LENGTH, sizeof (int));
		pwmQ = xQueueCreate( mainQUEUE_LENGTH, sizeof (int));
	
    if( cliRX != NULL )
    {

				// The UI Task is now created
			  xTaskCreate( UITASK, (signed char*) "UITASK",
										 150, NULL, MIN_TASK_PRIORITY, NULL);
				// the control task is now created
        xTaskCreate( controlTask, (signed char *) "CONTROLTASK",
                 configMINIMAL_STACK_SIZE, NULL, MIN_TASK_PRIORITY + 1, NULL );
				// the servo task is now created
        xTaskCreate( servoControl, (signed char *) "SERVOCONTROL",
                 configMINIMAL_STACK_SIZE, NULL, MIN_TASK_PRIORITY + 2, NULL );
        /* Start the tasks running. */
        vTaskStartScheduler();
    }

    /* If all is well we will never reach here as the scheduler will now be
    running.  If we do reach here then it is likely that there was insufficient
    heap available for the idle task to be created. */
    for( ;; )
        ;
}
/*-----------------------------------------------------------*/
static void UITASK( void *pvParameters )
{
	// constant values to make sure our cli task works without flaws
	char userInput [MAX_USER_INPUT] = "\0";
	char command [MAX_USER_INPUT];
	char subCommand [MAX_USER_INPUT];
	char ledNum [MAX_USER_INPUT];
	char expectEnd [1] ;
	int inputLength;
	unsigned parsingInput = 0;
	char temp;
	uint32_t temp1;
	int i;
	//initialize the next wake up time variable
	portTickType xNextWakeTime = xTaskGetTickCount();

	//function will act as if it is always running
	while(1)
	{
		// Until there is something is avaliable in the we keep blocking
		if (uxQueueMessagesWaiting(cliRX) == 1)
		{
			//recieve and input from the queue
			xQueueReceive (cliRX, &temp, RECIEVE_TIME);
			//preform CLI inputing and handling.
			inputLength = strLen (userInput);
			parsingInput = recieveInput(userInput, inputLength);
			if (parsingInput)
			{
				// All commands other than set will be handled within this task
				parseInput (userInput, command, ledNum, subCommand, expectEnd);
				if(handleInput (command, ledNum, subCommand, expectEnd))
				{
					// If handle input returns 1, it means they are trying to set height
					temp1 = strToInt (ledNum);
					// send desired set height to the ball if they picked set
					xQueueSend (ballSet, (void *) &temp1, 0);
					//delete message so next message can be entered

				}
				for(i = inputLength; i > 0; i--)
				{
					checkDelete (userInput, i, BACKSPACE);
				}
				inputLength = 0;
				printString (ENTER);
			}
			
		}
		vTaskDelayUntil(&xNextWakeTime, CLI_UPDATE_TIME);

	}
}

static void controlTask( void *pvParameters )
{
	uint32_t ballHeight;
	uint32_t ballSetHeight = 0;
	int difference;
	int temp;
	int gain;
	int level;
	
	portTickType xNextWakeTime = xTaskGetTickCount();

	while (1)
	{
		//Check to see if there is a new ball set value
		if (uxQueueMessagesWaiting(ballSet) == 1)
		{
			xQueueReceive (ballSet, &ballSetHeight, RECIEVE_TIME);
		}
		// start our adc capture
		adcStartCapture ();
		// Wait for there to be a value in our queue for the ADC value
		while (!(uxQueueMessagesWaiting(adcValueQ) == 1)) {}
		// Recieve the value from the queue
		xQueueReceive (adcValueQ, &ballHeight, RECIEVE_TIME);
			
		// Do our checks from the previous labs for setting/controlling ball height
		if(ballHeight < HIGH_RANGE)
			gain = HIGH_GAIN;
		else if (ballHeight < MID_RANGE)
			gain = MID_GAIN;
		else
			gain = LOW_GAIN;

		difference = (ballSetHeight) - ballHeight;
			
		temp = difference / gain;
		
		level = temp + NEUTRAL;
		// If level is outside limits, set to nearest limit
		// Once a level and case is decided we will then send our desired pwm
		// value to its specific queue
		if (level >= FULL_CLOSE)
		{
			level = FULL_CLOSE;
			xQueueSend( pwmQ, &level, NO_WAIT_TIME );
		}
		else if (level <= FULL_OPEN)
		{
			level = FULL_OPEN;
			xQueueSend( pwmQ, &level, NO_WAIT_TIME );
		}
		// Otherwise, set the raw ticks
		else
			xQueueSend( pwmQ, &level, NO_WAIT_TIME );
		
			vTaskDelayUntil(&xNextWakeTime, MIN_TIME_SERVO_UPDATE);
	}
	
}


static void servoControl( void *pvParameters )
{
	uint32_t pwmUpdate = 0;
	portTickType xNextWakeTime = xTaskGetTickCount();
    
	// Here we simply wait for a value for a new pwm value, and once we have it
	// We update it. 
    while (1) {
			if (uxQueueMessagesWaiting(pwmQ) == 1)
			{
				xQueueReceive (pwmQ, &pwmUpdate, 1000);
			}
			servoSetRaw (pwmUpdate);
        vTaskDelayUntil(&xNextWakeTime, MIN_TIME_SERVO_UPDATE);
    }

}


void USART2_IRQHandler ()
{
	// When the interrupt fires we will get the byte and send it to the cli queue
	// through the ISR queue send
	char temp;
	if (USART2_SR & USART_SR_RXNE)
	{
		USART2_SR &= ~USART_SR_RXNE;
	}
	temp = getByte ();
	xQueueSendFromISR (cliRX, (void *) &temp, NO_WAIT_TIME);
}

void ADC1_2_IRQHandler ()
{
	// When the ADC interrupt fires we will get the data and send it to our adc
	// queue through the ISR queue send
	uint16_t global_adc_1_value;

	global_adc_1_value = adcReadValue ();
	
	xQueueSendFromISR (adcValueQ, (void *) &global_adc_1_value, NO_WAIT_TIME);
}
