/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 &
File Name: main.c
Date Started: September 28, 2017 
Date Finished: October 12, 2017

Purpose: The purpose of this file is to make function calls that will allow
         our CLI interface to run seamlessly. Main itself will only be
				 responsible for declaring intial values, so that data may be
				 stored in them at a later point. 
				 
				 At a very high level our CLI interface works in three steps:
				 
				 1) Recieve input from the user
				 2) Parse/Analyze the recieved string into useable values
				 3) Take the cleaned up string, and preform a task if a valid
				    expression has been entered. Otherwise display error message

         For a more elaborate explination of allowed inputs, how the input
				 is parsed, and what valid expressions are please check the README.
				 Another option is to check the function decleration in its respective
				 .h file, there you will find a detailed explination of everything to
				 do with that function.
*/
#include "CLI.h"
#include "ledControl.h"

int main()
{	
	char userInput [MAX_USER_INPUT];
	char command [MAX_USER_INPUT];
	char subCommand [MAX_USER_INPUT];
	char ledNum [MAX_USER_INPUT];
	char expectEnd [1] ;
	
	openUsart();
	ledInit ();

	welcomeMessage();	
	
	while (1)
	{
		recieveInput (userInput);
		returnLine ();
		parseInput (userInput, command, ledNum, subCommand, expectEnd);
		handleInput (command, ledNum, subCommand, expectEnd);
	}
}


