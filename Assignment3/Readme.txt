Name: Arsalan Khan
SID: 200343820
Project: Assignment 3
Date: 2017/12/02

Purpose: This program will accept a user input, to use as a seed for our
         random function. It will then use that random value to go to a
	 location in our dictionary. Once there it will read out one word.
	 It will then generate a new random number, and print out the next,
	 until all 5 words have been outputted.

Folders:
1) A3-keil: This file will ask a user for input, to utilize as a seed for the
            random number generator
            
2) A3-snoopy: This file will make use of the time(0) function to seed the random
              number generator

Functions:

name: randomWordGet
input: uint32_t -> This is because a 32 bit uint will be able to represent all
       possible positions in our charcter array
output: print a word to the screen
returns: none
---------------------------
name: randomNumGen
input: pointer to a uint32_t
output: none
returns: none
purpose: Assigns a value to the pointer passed in.


How the dictionary was created:
I took the file provided by Karim and went to snoopy. In snoopy I used the following commands

grep -v ".*'.*$" american-english > no-possessive
grep -P -v '[^\x00-\x7f]' no-possessive > no-unicode
awk 'length>5' no-unicode > trimmed-words

Once all of the 's, non-unicode values, and words of length 5 and below were trimmed I took to
python. The python file can be found in the A3-snoopy folder. You may note that i am writing
/n instead of \n, this is because when i wrote \n it just went to a new line in the file. 
Therefore I used /n and then did a find/replace on the generated h file. I also kept count
of the total characters in the dictionary to create my .h variables. I slightly modified the
generated file to make it suitable to be used as a .h file

fileR = open("trimmed-words", "r")
fileW = open("final.txt", "w")
count = 0
for line in fileR:
    for character in line:
        if(character == '\n'):
            fileW.write("'/n',")
            count = count + 1
            fileW.write("\n")
        else:            
            fileW.write("'")
            fileW.write(character)
            count = count + 1
            fileW.write("',")
            
fileW.write(str(count))
fileR.close()
fileW.close()


Program Data:
RW-data = 16
This tells us our RAM usage 

ZI-data = 2112
This tells us our bss usage





