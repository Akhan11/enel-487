/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: servo.h
Date Started: November 2, 2017 
Date Finished: November 9, 2017 

Purpose: This file contains function definition for a control system to set
         the height of the ping-pong ball
*/

#include "controls.h"

void setBallFloat (uint32_t cm)
{
	if (cm < MAX_CM && (cm > MIN_CM))
		global_control_set_ball_raw = cmToRaw (cm);
	else
		global_control_set_ball_raw = 0;
}

uint32_t cmToRaw (uint32_t cm)
{
	return (CONVERSION_SCALAR / cm) + CONVERSION_SCALAR;
}


void updateController ()
{
	int difference;
	int temp;
	int gain;
	
	// if 0, stop updating
	if (global_control_set_ball_raw != 0)
	{
		adcStartCapture ();
		
		while (!global_adc_new_value) {}
			
		// Update the gain depending on the set position
		
		if(global_control_set_ball_raw < HIGH_RANGE)
			gain = HIGH_GAIN;
		else if (global_control_set_ball_raw < MID_RANGE)
			gain = MID_GAIN;
		else
			gain = LOW_GAIN;
		
		difference = (global_control_set_ball_raw) - global_adc_1_value;
			
		temp = difference / gain;
		setControlLevel (temp);

	}
}

void setControlLevel (int value)
{
	
	int level = value + NEUTRAL;
	// If level is outside limits, set to nearest limit
	if (level >= FULL_CLOSE)
		servoSetRaw (FULL_CLOSE);
	
	else if (level <= FULL_OPEN)
		servoSetRaw (FULL_OPEN);
	
	// Otherwise, set the raw ticks
	else
		servoSetRaw (level);
}
