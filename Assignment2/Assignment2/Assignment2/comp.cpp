/**
Name: Arsalan Khan
SID: 200343820
Date Created: October 03, 2017
Purpose: The purpose of this file is to simply contain fleshed out functions for the definitions
		 made in comp.h. This file contains the muscle power of this assignment, without it none
		 of this stuff would even come close to running.


/////////////////////////////////////////////////////////////////////////////////////////////////////////
NOTE
//
You will note that in a lot of these functions i deliberatly cast all values to floats. This is to avoid
any warnings that may appear due to ambiguous operations.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////


*/
#include <iostream>
using namespace std;
#include "comp.h"
#include "math.h"


/**
Name: addition 
Input: two complex numbers (struct definition given in comp.h)
Output: one complex number 
Purpose: The purpose of this function is to do an addition of any given two complex numbers
*/
Complex complexAddition(Complex input1, Complex input2)
{
	Complex result;

	result.real = input1.real + input2.real;
	result.imaginary = input1.imaginary + input2.imaginary;

	return result;
}

/**
Name: subtract
Input: two complex numbers (struct definition given in comp.h)
Output: one complex number
Purpose: The purpose of this function is to a subtraction of any given two complex numbers.
*/
Complex complexSubtract(Complex input1, Complex input2)
{
	Complex result;

	result.real = input1.real - input2.real;
	result.imaginary = input1.imaginary - input2.imaginary;

	return result;
}

/**
Name: multiply
Input: two complex numbers (struct definition given in comp.h)
Output: one complex number
Purpose: The purpose of this function is take two given inputs, subtract them and return the multiple.
		 The method of multiplying complex numbers is very similar to the FOIL method used in math. 
		 That method is simply applied here to the real and imaginary parts.
*/
Complex complexMultiply(Complex input1, Complex input2)
{
	Complex result;

	result.real = ((input1.real * input2.real) - (input1.imaginary * input2.imaginary));
	result.imaginary = ((input1.real * input2.imaginary) + (input1.imaginary * input2.real));

	return result;
}

/**
Name: divide
Input: two complex numbers (struct definition given in comp.h)
Output: one complex number
Purpose: The purpose of this function is take two given inputs, subtract them and return the quotient.
		 The method of dividing complex numbers is similar to multiplying them, expect with a few additional steps.
		 The additional step is to divide by the double of the inversed input2. So (I1 * I2)/2*I2
*/
Complex complexDivide(Complex input1, Complex input2)
{
	Complex result, temp;

	input2.imaginary = -input2.imaginary;

	temp = multiply(input1, input2);

	result.real = (temp.real / ((input2.real * input2.real) + (input2.imaginary * input2.imaginary)));
	result.imaginary = (temp.imaginary / ((input2.real * input2.real) + (input2.imaginary * input2.imaginary)));

	return result;
}

/**
Name: abs
Input: single complex number (struct definition given in comp.h)
Output: floating point representation of the absolute value
Purpose: the purpose of this function is to return the absolute value of any given complex number.
		 this can be obtained by taking the square root of the sum of squares in the complex number.
		 |x + jy| = (x^2 + y^2)^1/2
*/
float complexAbs(Complex input1)
{
	return float(sqrt(pow(input1.real, 2) + pow(input1.imaginary, 2)));
}

/**
Name: arg 
Input: single complex number (struct definition given in comp.h)
Output: floating point representation of the angle of a given complex number in radians
Purpose: The purpose of this function is to return the angle of any given complex number, represented as a radian.
		 We can determine the angle simply by taking the inverse tan of the real and imaginary values.
		 <(x + jy) = tan-1(y/x)
*/
float complexArg(Complex input1)
{
	return float(atan(input1.imaginary / input1.real));
}

/**
Name: argDeg
Input: single complex number (struct definition given in comp.h)
Output: floating point representation of the angle of a given complex number in degrees
Purpose: The purpose of this function is to return the angle of any given complex number, represented in degrees.
		 We can determine the angle simply by taking the inverse tan of the real and imaginary values.
		 <(x + jy) = tan-1(y/x) -> radian value, once we have this we can do a simple conversion to change it to degrees.
		 x rad * 180/pi = x deg
*/
float complexArgDeg(Complex input1)
{
	float temp;
	temp = float(atan(input1.imaginary / input1.real));
	temp = float(temp * (DEG_CONVERSION / PI));
	return temp;
}

/**
Name: exp
Input: single complex number (struct defiction given in comp.h)
Output: single complex number
Purpose: The purpose of this function is to calculate the exponential of a given complex number.
		 e^(x+jy) = (e^x) * (cos(y) + jsin(y))
*/
Complex complexExp(Complex input1)
{
	Complex temp;
	float scalarMultiple;
	scalarMultiple = float(exp(input1.real));
	temp.real = float(scalarMultiple * cos(input1.imaginary));
	temp.imaginary = float(scalarMultiple * sin(input1.imaginary));
	return temp;
}

/**
Name: inv
Input: singl;e complex number (struct defintion given in comp.h)
Output: single complex number
Purpose: the purpose of this function is to give the inverse of any given complex number, 1/(z+jy).
		 the simplest way of achiving this is to divide 1 by whatever the complex number is. 
*/
Complex complexInv(Complex input1) {
	Complex temp = { 1, 0 };
	return divide(temp, input1);
}

/**
Name: printValue
Input: two bool values indicating what needs to be output, and two numeric entities which are to be outputed.
Output: None
Purpose: The purpose of this function is to simply remove printing commands from the main body of the program.
		 The logic is fairly simple, we can either have a result that is a single floating point number, or another
		 which is a complex number. This function simply determines which, by using three checks.

		 Is the single result flag set?
		 Is the correctOperator flag set?
		 Is the imaginary portion of the complex number positive?
*/
void printValue(bool correctOperator, bool singleResult, Complex result, float altResult)
{

	if (!singleResult)
	{
		if (result.imaginary >= 0 && correctOperator)
		{
			cout << result.real << " + j " << result.imaginary;
		}
		else if (result.imaginary < 0 && correctOperator)
		{
			result.imaginary = -result.imaginary;
			cout << result.real << " - j " << result.imaginary;
		}
	}
	else if (singleResult)
	{
		cout << altResult;
	}
	else 
	{
		cerr << "Not the correct input";
	}
}