/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 & 200337734
File Name: timer_impl.h
Date Started: October 12, 2017 
Date Finished: October 18, 2017 

Purpose: The purpose of this file is to hold all declerations for items
         related to our initilization of TIM2 & TIM3. In addition to that
				 we have also included variables that help with our operations for
				 converting int -> uint and various other things. We also declare
				 our struct definitions here. It should be noted that there is no
				 1024 Byte struct declared. This is because the struct is made by
				 using 8, 128 Byte structs.
*/
#pragma once
#include "stdint.h"
#define TIM2_CR1_DIR      0x0010
#define TIM2_CR1_CEN      0x0001
#define TIM2_ARR_MAX      0xFFFF
#define TIM2_ARR_10K      0x2710
#define TIM2_EN           0x0001
#define TIM2_DIER_UPDT    0x0001
#define TIM2_PSC_7K2      0x1C1F
#define TIM3_EN           0x0002
#define TIM4_EN           0x0004
#define NVIC_BIT_29       0x20000000

#define TIM4_PSC_500KHZ   143
#define TIM4_ARR_10K      9999
#define TIM4_CCMR_CH2_PWM 0x6000
#define TIM4_CCER_CH2_OUT 0x0010
#define TIM4_CR1_URS      0x0004
#define TIM4_CR1_CEN      0x0001

#define MIN_TICKS         360
#define MAX_TICKS         1200

#define BIT15             0x8000
#define TWO_POW_16        65536

#define ARRAY_SIZE        2
#define NUM_OF_TRIALS     5000
#define NUM_OF_TESTS      9
#define BYTE_8_128        16
#define BYTE_128_1024     8

// decleration of our struct of 8 Bytes
struct Byte_8
{
	int64_t bits;
};

// Decleration of our struct of 128 Bytes
struct Byte_128
{
	struct Byte_8 bits[BYTE_8_128];
};

typedef struct Byte_8 Byte_8;
typedef struct Byte_128 Byte_128;
