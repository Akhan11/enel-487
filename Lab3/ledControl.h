/**
Name: Arsalan Khan & Isaiah Peters
SID: 200343820 &
File Name: ledControl.h
Date Started: September 28, 2017 
Date Finished: September 28, 2017 

Purpose: The purpose of ledControl.h is to hold function headers/declerations
         for all things related to controlling the LEDs on the STM board.
				 We also define an array of different numbers, these numbers were 
				 selected by referring to the BSRR register map in the STM reference
				 manual. Each value is fairly descriptive, but below is an example
				 of how we obtained these values.
				 
				 PB8_ON -> 0x00000100, this is a 32bit number of all 0's except for
				           position 9. Next refer to page 172 of the reference manual
									 rev 16. We will see that the 9th position is mapped to BS8.
									 This register when set will put PB8 high, and if an LED
									 is attached then it will turn on.
									 
				 PB8_OFF -> 0x01000000, this is a 32 bit number of all 0's except for
				            postiion 24. Once again refer to page 172 of ref manual 
										rev 16. We will see that the 24th position is mapped to BR8
										, when set this will put PB8 low, and if an LED is attached
										it will turn off.

All values indicated below were pulled from the STM reference manual rev 16.
*/
#pragma once

#define APB2ENR_GPIOB            ((uint32_t)0x00000008)
#define GPIOB_OFF                ((uint32_t)0x0000FF00)
#define OUT_TYPE                 ((uint32_t)0x33333333)
#define PB8_ON                   ((uint32_t)0x00000100)
#define PB8_OFF                  ((uint32_t)0x01000000)
#define PB9_ON                   ((uint32_t)0x00000200)
#define PB9_OFF                  ((uint32_t)0x02000000)
#define PB10_ON                  ((uint32_t)0x00000400)
#define PB10_OFF                 ((uint32_t)0x04000000)
#define PB11_ON                  ((uint32_t)0x00000800)
#define PB11_OFF                 ((uint32_t)0x08000000)
#define PB12_ON                  ((uint32_t)0x00001000)
#define PB12_OFF                 ((uint32_t)0x10000000)
#define PB13_ON                  ((uint32_t)0x00002000)
#define PB13_OFF                 ((uint32_t)0x20000000)
#define PB14_ON                  ((uint32_t)0x00004000)
#define PB14_OFF                 ((uint32_t)0x40000000)
#define PB15_ON                  ((uint32_t)0x00008000)
#define PB15_OFF                 ((uint32_t)0x80000000)
#define PBALL_ON                 ((uint32_t)0x0000ff00)
#define PBALL_OFF                ((uint32_t)0xff000000)
#define LED_8                    8
#define LED_9                    9
#define LED_10                   10
#define LED_11                   11
#define LED_12                   12
#define LED_13                   13
#define LED_14                   14
#define LED_15                   15
#define NUM_LED                  8


/**
Name: ledInit 
Input: None
Output: None
Purpose: The purpose of this function is to intialize clocks related to
         our LED bus (port B), and configure the targeted LEDS as outputs.
*/
void ledInit (void);

/**
Name: ledOn 
Input: One unsigned
Output: None
Purpose: This function will turn on an LED indicated by the unsigned input
*/
void ledOn (unsigned);
/**
Name: allLedOn 
Input: None
Output: None
Purpose: This function will turn on all LEDS (PB8-PB15)
*/
void allLedOn (void);

/**
Name: ledOff 
Input: One unsigned
Output: None
Purpose: This function will turn off an LED indicated by the unsigned input
*/
void ledOff (unsigned);
/**
Name: allLedOff 
Input: None
Output: None
Purpose: This function will turn off all LEDS (PB8-PB15)
*/
void allLedOff (void);

/**
Name: queryLed 
Input: One unsigned
Output: None
Purpose: This function will check if the LED indicated by the unsigned input
         is on or off.
*/
unsigned queryLed (unsigned);

/**
Name: queryLed 
Input: One unsigned
Output: None
Purpose: This function will check if all LEDs indicated by the unsigned input
         is on or off.
*/
unsigned queryAllLed (void);
